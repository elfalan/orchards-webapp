module.exports = [
    {
      "id": "EP3ORT",
      "farm": "Leroux Creek",
      "label": "Farm 5",
      "lat": 38.840737,
      "lng": -107.892950
    }, {
      "id": "O51215",
      "farm": "Leroux Creek",
      "label": "Farm 5",
      "lat": 38.838811,
      "lng": -107.896365
    }, {
      "id": "ENIUZK",
      "farm": "Leroux Creek",
      "label": "Farm 5",
      "lat": 38.836198,
      "lng": -107.902020
    }, {
      "id": "253MOH",
      "farm": "Leroux Creek",
      "label": "Farm 5",
      "lat": 38.835792,
      "lng": -107.899000
    }, {
      "id": "03WUSX",
      "farm": "Leroux Creek",
      "label": "Farm 5",
      "lat": 38.834527,
      "lng": -107.901822
    }, {
      "id": "UV6ALW",
      "farm": "Leroux Creek",
      "label": "Farm 5",
      "lat": 38.833554,
      "lng": -107.897721
    }, {
      "id": "UINMPN",
      "farm": "Leroux Creek",
      "label": "Farm 5",
      "lat": 38.833719,
      "lng": -107.894803
    }, {
      "id": "SN9BNN",
      "farm": "Leroux Creek",
      "label": "Spruce",
      "lat": 38.832010,
      "lng": -107.893285
    }, {
      "id": "EMY75M",
      "farm": "Leroux Creek",
      "label": "Spruce",
      "lat": 38.830399,
      "lng": -107.893063
    }, {
      "id": "FMZO13",
      "farm": "Leroux Creek",
      "label": "Farm 5",
      "lat": 38.837269,
      "lng": -107.894846
    }, {
      "id": "Z5DYHL",
      "farm": "Leroux Creek",
      "label": "EME",
      "lat": 38.811287,
      "lng": -107.774029
    }, {
      "id": "D9B8MJ",
      "farm": "Leroux Creek",
      "label": "Tembrock",
      "lat": 38.808114,
      "lng": -107.782903
    }, {
      "id": "V8CDUF",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel D",
      "lat": 38.809885,
      "lng": -107.771978
    }, {
      "id": "9HV2AA",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel A",
      "lat": 38.813651,
      "lng": -107.781520
    }, {
      "id": "Y9RLH9",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel A",
      "lat": 38.812738,
      "lng": -107.780040
    }, {
      "id": "64GVM7",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel A",
      "lat": 38.812681,
      "lng": -107.778369
    }, {
      "id": "FS451Z",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel A",
      "lat": 38.811600,
      "lng": -107.776983
    }, {
      "id": "RU1ITZ",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel A",
      "lat": 38.811616,
      "lng": -107.778280
    }, {
      "id": "DG4O6F",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel A",
      "lat": 38.811607,
      "lng": -107.780422
    }, {
      "id": "BJ76OM",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel D",
      "lat": 38.809805,
      "lng": -107.767600
    }, {
      "id": "CNMGKL",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel B",
      "lat": 38.819062,
      "lng": -107.781481
    }, {
      "id": "IQRK7T",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel D",
      "lat": 38.809749,
      "lng": -107.769018
    }, {
      "id": "KP55FE",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel D",
      "lat": 38.808574,
      "lng": -107.769947
    }, {
      "id": "CUPV5D",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel C",
      "lat": 38.811359,
      "lng": -107.767887
    }, {
      "id": "HG8IBG",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel C",
      "lat": 38.814443,
      "lng": -107.768203
    }, {
      "id": "C7EULH",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel B",
      "lat": 38.819133,
      "lng": -107.779528
    }, {
      "id": "NSFE7R",
      "farm": "Leroux Creek",
      "label": "Stechert",
      "lat": 38.819289,
      "lng": -107.799494
    }, {
      "id": "QOZOKO",
      "farm": "Leroux Creek",
      "label": "Stechert",
      "lat": 38.820134,
      "lng": -107.796215
    }, {
      "id": "G2RRJC",
      "farm": "Leroux Creek",
      "label": "EME",
      "lat": 38.807784,
      "lng": -107.773864
    }, {
      "id": "D5XN92",
      "farm": "Leroux Creek",
      "label": "Tembrock",
      "lat": 38.806159,
      "lng": -107.784878
    }, {
      "id": "GA480B",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel B",
      "lat": 38.818970,
      "lng": -107.776992
    }, {
      "id": "MU54LE",
      "farm": "Leroux Creek",
      "label": "Farm 4",
      "lat": 38.798205,
      "lng": -107.784534
    }, {
      "id": "VVUD8V",
      "farm": "Leroux Creek",
      "label": "Farm 4",
      "lat": 38.800351,
      "lng": -107.784594
    }, {
      "id": "0ADGCW",
      "farm": "Leroux Creek",
      "label": "Honeycrisp Parcel D",
      "lat": 38.808580,
      "lng": -107.772137
    }, {
      "id": "UNCVO1",
      "farm": "Cameron",
      "label": "B3A",
      "lat": 39.111170,
      "lng": -108.320485
    }, {
      "id": "2M3VN4",
      "farm": "Cameron",
      "label": "B4",
      "lat": 39.111867,
      "lng": -108.332144
    }, {
      "id": "IBICZD",
      "farm": "Kaibab",
      "label": "Forte",
      "lat": 39.098223,
      "lng": -108.343520
    }, {
      "id": "FYNN89",
      "farm": "Kaibab",
      "label": "Jackson",
      "lat": 39.099882,
      "lng": -108.328834
    }, {
      "id": "O02VT7",
      "farm": "Kaibab",
      "label": "Snow",
      "lat": 39.092243,
      "lng": -108.409132
    }, {
      "id": "AR5SEY",
      "farm": "Kokopeli",
      "label": "Bowman North",
      "lat": 39.115519,
      "lng": -108.362646
    }, {
      "id": "CDF20O",
      "farm": "Kokopeli",
      "label": "Bowman South",
      "lat": 39.112457,
      "lng": -108.366633
    }, {
      "id": "9WKDCB",
      "farm": "Peach Haven",
      "label": "North",
      "lat": 39.111441,
      "lng": -108.371302
    }, {
      "id": "99KXAO",
      "farm": "Ruckman",
      "label": "9 acre",
      "lat": 39.086368,
      "lng": -108.382533
    }, {
      "id": "F6QAO7",
      "farm": "Ruckman",
      "label": "Bobs",
      "lat": 39.067586,
      "lng": -108.410743
    }, {
      "id": "XZEQ4V",
      "farm": "Ruckman",
      "label": "Home",
      "lat": 39.084431,
      "lng": -108.389822
    }, {
      "id": "HL5E7G",
      "farm": "Sowell",
      "label": "Chris",
      "lat": 39.056477,
      "lng": -108.413951
    }, {
      "id": "XBTSZV",
      "farm": "Sowell",
      "label": "Cole",
      "lat": 39.054035,
      "lng": -108.433398
    }, {
      "id": "646WFB",
      "farm": "Sowell",
      "label": "Kenny",
      "lat": 39.049423,
      "lng": -108.434455
    }, {
      "id": "0VB0NK",
      "farm": "Talbott Farms",
      "label": "Taylor",
      "lat": 39.100179,
      "lng": -108.393327
    }, {
      "id": "TGR5D0",
      "farm": "Talbott Farms",
      "label": "60",
      "lat": 39.108160,
      "lng": -108.403893
    }, {
      "id": "ETC5S0",
      "farm": "Talbott Farms",
      "label": "Mechau",
      "lat": 39.100890,
      "lng": -108.395738
    }, {
      "id": "CSFE5E",
      "farm": "Talbott Farms",
      "label": "Kay",
      "lat": 39.083108,
      "lng": -108.420295
    }, {
      "id": "GN0PV0",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.076500,
      "lng": -108.399263
    }, {
      "id": "4XDB42",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.075995,
      "lng": -108.399645
    }, {
      "id": "KRJPWV",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.078289,
      "lng": -108.399302
    }, {
      "id": "B2A6KN",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.079159,
      "lng": -108.399017
    }, {
      "id": "EIAP2T",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.058604,
      "lng": -108.435455
    }, {
      "id": "EZRTC6",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.059442,
      "lng": -108.433294
    }, {
      "id": "BWDOY2",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.059508,
      "lng": -108.440198
    }, {
      "id": "DKX0PD",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.056645,
      "lng": -108.440651
    }, {
      "id": "TAAL1O",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.055888,
      "lng": -108.438040
    }, {
      "id": "UYVCYI",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.056013,
      "lng": -108.435234
    }, {
      "id": "TW853A",
      "farm": "Fuller Orchards",
      "label": "TBD",
      "lat": 39.056889,
      "lng": -108.431767
    }, {
      "id": "18ZC55",
      "farm": "Talbott Farms",
      "label": "Bonzek",
      "lat": 39.053055,
      "lng": -108.436484
    }, {
      "id": "8CR27Y",
      "farm": "Talbott Farms",
      "label": "Bikki",
      "lat": 39.111079,
      "lng": -108.338680
    }, {
      "id": "3YG5OE",
      "farm": "Talbott Farms",
      "label": "Wallace",
      "lat": 39.088630,
      "lng": -108.376853
    }, {
      "id": "IRMRC8",
      "farm": "Talbott Farms",
      "label": "1 South",
      "lat": 39.092760,
      "lng": -108.343033
    }, {
      "id": "ZTL89K",
      "farm": "Talbott Farms",
      "label": "9",
      "lat": 39.095995,
      "lng": -108.357120
    }, {
      "id": "GNMCD5",
      "farm": "Talbott Farms",
      "label": "23",
      "lat": 39.073931,
      "lng": -108.410991
    }, {
      "id": "OZTUJH",
      "farm": "Talbott Farms",
      "label": "Riverview",
      "lat": 39.096057,
      "lng": -108.364281
    }, {
      "id": "G0B221",
      "farm": "Talbott Farms",
      "label": "Wick West",
      "lat": 39.073759,
      "lng": -108.415174
    }, {
      "id": "ISDJU4",
      "farm": "Kropp",
      "label": "Cooler",
      "lat": 38.865296,
      "lng": -107.604203
    }, {
      "id": "UQUO7G",
      "farm": "Kropp",
      "label": "Home Place",
      "lat": 38.873895,
      "lng": -107.620533
    }, {
      "id": "BNNP5L",
      "farm": "Kropp",
      "label": "VV Apricots",
      "lat": 38.858145,
      "lng": -107.605015
    }, {
      "id": "NWIVVT",
      "farm": "Kropp",
      "label": "Wilson Skeena",
      "lat": 38.859825,
      "lng": -107.610774
    }, {
      "id": "ZP7ULN",
      "farm": "Alvey",
      "label": "Gala",
      "lat": 38.826128,
      "lng": -107.785341
    }, {
      "id": "HOT4ZI",
      "farm": "Alvey",
      "label": "Fuji",
      "lat": 38.823658,
      "lng": -107.783712
    }, {
      "id": "17BXBQ",
      "farm": "Topp Fruits",
      "label": "TBD",
      "lat": 38.879585,
      "lng": -107.601073
    }, {
      "id": "9MJ7P6",
      "farm": "Topp Fruits",
      "label": "TBD",
      "lat": 38.879720,
      "lng": -107.599473
    }, {
      "id": "LN4NCU",
      "farm": "Topp Fruits",
      "label": "TBD",
      "lat": 38.882295,
      "lng": -107.599914
    }, {
      "id": "3EUWFD",
      "farm": "Topp Fruits",
      "label": "TBD",
      "lat": 38.882275,
      "lng": -107.601077
    }, {
      "id": "FEM090",
      "farm": "Topp Fruits",
      "label": "TBD",
      "lat": 38.881432,
      "lng": -107.600049
    }, {
      "id": "B106NA",
      "farm": "Leroux Creek",
      "label": "Farm 5 - Tower",
      "lat": 38.835808,
      "lng": -107.899026
    }, {
      "id": "YYYCSH",
      "farm": "Leroux Creek",
      "label": "Spruce - Tower",
      "lat": 38.832010,
      "lng": -107.893285
    }, {
      "id": "R0W8ND",
      "farm": "Leroux Creek",
      "label": "Farm 5 - Tower",
      "lat": 38.837270,
      "lng": -107.894824
    }, {
      "id": "R12VWN",
      "farm": "Ela Family Farms",
      "label": "Plot BC",
      "lat": 38.817969,
      "lng": -107.784942
    }, {
      "id": "PPTKQ6",
      "farm": "Ela Family Farms",
      "label": "Plot T",
      "lat": 38.816601,
      "lng": -107.786067
    }, {
      "id": "BLMQB0",
      "farm": "Leroux Creek",
      "label": "Farm 5 North Edge",
      "lat": 38.842182,
      "lng": -107.889215
    }, {
      "id": "ZVZLX9",
      "farm": "Leroux Creek",
      "label": "Farm 5 Southwest Edge",
      "lat": 38.834478,
      "lng": -107.905892
    }, {
      "id": "LUGQBL",
      "farm": "Leroux Creek",
      "label": "Spruce Southeast Corner",
      "lat": 38.828057,
      "lng": -107.894612
    }, {
      "id": "J7QLHO",
      "farm": "Leroux Creek",
      "label": "Stechert",
      "lat": 38.822454,
      "lng": -107.795673
    }, {
      "id": "16NFDT",
      "farm": "Leroux Creek",
      "label": "Farm 4",
      "lat": 38.796464,
      "lng": -107.785898
    }, {
      "id": "4R3OVS",
      "farm": "Leroux Creek",
      "label": "Parcel D",
      "lat": 38.808502,
      "lng": -107.767706
    }, {
      "id": "00SUN1",
      "farm": "Ela Family Farms",
      "label": "P Block",
      "lat": 38.816743,
      "lng": -107.782709
    }, {
      "id": "E7Y1NM",
      "farm": "Ela Family Farms",
      "label": "N Block",
      "lat": 38.816330,
      "lng": -107.793882
    }, {
      "id": "EJFCH6",
      "farm": "Ela Family Farms",
      "label": "K Block",
      "lat": 38.817676,
      "lng": -107.790742
    }, {
      "id": "OM9MFY",
      "farm": "Osito",
      "label": "Cherry High Tunnels",
      "lat": 38.825208,
      "lng": -107.780718
    }, {
      "id": "FIY0HK",
      "farm": "Osito",
      "label": "Apple Trees",
      "lat": 38.821862,
      "lng": -107.779167
    }, {
      "id": "KDSB9Q",
      "farm": "Yvom",
      "label": "Vineyard",
      "lat": 38.836657,
      "lng": -107.779822
    }, {
      "id": "31PQ0E",
      "farm": "Cameron",
      "label": "B72016",
      "lat": 39.106049,
      "lng": -108.325282
    }, {
      "id": "2FI6RE",
      "farm": "Peach Haven",
      "label": "South",
      "lat": 39.108886,
      "lng": -108.370643
    }
  ];