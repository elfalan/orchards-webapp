module.exports = [
    {
      "id": 300,
      "sensor_id": "16NFDT",
      "timestamp": 1525733888,
      "tempAir": 88,
      "humAir": 11,
      "type": 0
    },
    {
      "id": 301,
      "sensor_id": "16NFDT",
      "timestamp": 1525734039,
      "tempAir": 91.6,
      "humAir": 8.5,
      "type": 0
    },
    {
      "id": 302,
      "sensor_id": "16NFDT",
      "timestamp": 1525734190,
      "tempAir": 85.6,
      "humAir": 11.5,
      "type": 0
    },
    {
      "id": 303,
      "sensor_id": "16NFDT",
      "timestamp": 1525734341,
      "tempAir": 84.7,
      "humAir": 12,
      "type": 0
    },
    {
      "id": 1202,
      "sensor_id": "16NFDT",
      "timestamp": 1525736254,
      "tempAir": 81.3,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 3160,
      "sensor_id": "16NFDT",
      "timestamp": 1525741697,
      "tempAir": 76.3,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 3161,
      "sensor_id": "16NFDT",
      "timestamp": 1525741848,
      "tempAir": 75.7,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 3840,
      "sensor_id": "16NFDT",
      "timestamp": 1525743768,
      "tempAir": 73.8,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 2492,
      "sensor_id": "16NFDT",
      "timestamp": 1525740082,
      "tempAir": 77,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 1847,
      "sensor_id": "16NFDT",
      "timestamp": 1525738166,
      "tempAir": 79.5,
      "humAir": 13,
      "type": 0
    },
    {
      "id": 2491,
      "sensor_id": "16NFDT",
      "timestamp": 1525739931,
      "tempAir": 77.5,
      "humAir": 13,
      "type": 0
    },
    {
      "id": 3162,
      "sensor_id": "16NFDT",
      "timestamp": 1525741999,
      "tempAir": 75.6,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 3838,
      "sensor_id": "16NFDT",
      "timestamp": 1525743466,
      "tempAir": 75,
      "humAir": 14.5,
      "type": 0
    },
    {
      "id": 3839,
      "sensor_id": "16NFDT",
      "timestamp": 1525743617,
      "tempAir": 74.3,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 3841,
      "sensor_id": "16NFDT",
      "timestamp": 1525743919,
      "tempAir": 73.6,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 4508,
      "sensor_id": "16NFDT",
      "timestamp": 1525745388,
      "tempAir": 73.2,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 4510,
      "sensor_id": "16NFDT",
      "timestamp": 1525745690,
      "tempAir": 72.9,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 4509,
      "sensor_id": "16NFDT",
      "timestamp": 1525745539,
      "tempAir": 73.2,
      "humAir": 15.5,
      "type": 0
    },
    {
      "id": 6088,
      "sensor_id": "16NFDT",
      "timestamp": 1525749686,
      "tempAir": 63.3,
      "humAir": 24.5,
      "type": 0
    },
    {
      "id": 4730,
      "sensor_id": "16NFDT",
      "timestamp": 1525745841,
      "tempAir": 72.3,
      "humAir": 15.5,
      "type": 0
    },
    {
      "id": 6805,
      "sensor_id": "16NFDT",
      "timestamp": 1525751607,
      "tempAir": 61.2,
      "humAir": 29.5,
      "type": 0
    },
    {
      "id": 6087,
      "sensor_id": "16NFDT",
      "timestamp": 1525749535,
      "tempAir": 65.5,
      "humAir": 22,
      "type": 0
    },
    {
      "id": 6806,
      "sensor_id": "16NFDT",
      "timestamp": 1525751456,
      "tempAir": 60.8,
      "humAir": 30.5,
      "type": 0
    },
    {
      "id": 6804,
      "sensor_id": "16NFDT",
      "timestamp": 1525751305,
      "tempAir": 61.2,
      "humAir": 29.5,
      "type": 0
    },
    {
      "id": 5405,
      "sensor_id": "16NFDT",
      "timestamp": 1525747764,
      "tempAir": 71.6,
      "humAir": 16,
      "type": 0
    },
    {
      "id": 7548,
      "sensor_id": "16NFDT",
      "timestamp": 1525753227,
      "tempAir": 63,
      "humAir": 23,
      "type": 0
    },
    {
      "id": 7547,
      "sensor_id": "16NFDT",
      "timestamp": 1525753076,
      "tempAir": 61,
      "humAir": 30.5,
      "type": 0
    },
    {
      "id": 7549,
      "sensor_id": "16NFDT",
      "timestamp": 1525753378,
      "tempAir": 65.5,
      "humAir": 19.5,
      "type": 0
    },
    {
      "id": 7550,
      "sensor_id": "16NFDT",
      "timestamp": 1525753529,
      "tempAir": 64.9,
      "humAir": 20.5,
      "type": 0
    },
    {
      "id": 8582,
      "sensor_id": "16NFDT",
      "timestamp": 1525755452,
      "tempAir": 62.2,
      "humAir": 28.5,
      "type": 0
    },
    {
      "id": 10103,
      "sensor_id": "16NFDT",
      "timestamp": 1525759143,
      "tempAir": 59,
      "humAir": 32,
      "type": 0
    },
    {
      "id": 10104,
      "sensor_id": "16NFDT",
      "timestamp": 1525759294,
      "tempAir": 59.4,
      "humAir": 30.5,
      "type": 0
    },
    {
      "id": 9352,
      "sensor_id": "16NFDT",
      "timestamp": 1525757372,
      "tempAir": 61.3,
      "humAir": 28,
      "type": 0
    },
    {
      "id": 13185,
      "sensor_id": "16NFDT",
      "timestamp": 1525766853,
      "tempAir": 51.6,
      "humAir": 38,
      "type": 0
    },
    {
      "id": 10811,
      "sensor_id": "16NFDT",
      "timestamp": 1525761064,
      "tempAir": 59.2,
      "humAir": 30,
      "type": 0
    },
    {
      "id": 12446,
      "sensor_id": "16NFDT",
      "timestamp": 1525765072,
      "tempAir": 53.1,
      "humAir": 36.5,
      "type": 0
    },
    {
      "id": 10810,
      "sensor_id": "16NFDT",
      "timestamp": 1525760913,
      "tempAir": 59.7,
      "humAir": 29.5,
      "type": 0
    },
    {
      "id": 10812,
      "sensor_id": "16NFDT",
      "timestamp": 1525761215,
      "tempAir": 58.1,
      "humAir": 31.5,
      "type": 0
    },
    {
      "id": 11520,
      "sensor_id": "16NFDT",
      "timestamp": 1525762991,
      "tempAir": 55.6,
      "humAir": 34,
      "type": 0
    },
    {
      "id": 11518,
      "sensor_id": "16NFDT",
      "timestamp": 1525762689,
      "tempAir": 56.7,
      "humAir": 33.5,
      "type": 0
    },
    {
      "id": 11519,
      "sensor_id": "16NFDT",
      "timestamp": 1525762840,
      "tempAir": 56.3,
      "humAir": 35,
      "type": 0
    },
    {
      "id": 11521,
      "sensor_id": "16NFDT",
      "timestamp": 1525763142,
      "tempAir": 54.7,
      "humAir": 36.5,
      "type": 0
    },
    {
      "id": 13184,
      "sensor_id": "16NFDT",
      "timestamp": 1525767004,
      "tempAir": 51.1,
      "humAir": 39.5,
      "type": 0
    },
    {
      "id": 20351,
      "sensor_id": "16NFDT",
      "timestamp": 1525784385,
      "tempAir": 52.9,
      "humAir": 41.5,
      "type": 0
    },
    {
      "id": 18058,
      "sensor_id": "16NFDT",
      "timestamp": 1525778425,
      "tempAir": 45.9,
      "humAir": 48,
      "type": 0
    },
    {
      "id": 13945,
      "sensor_id": "16NFDT",
      "timestamp": 1525768779,
      "tempAir": 49.1,
      "humAir": 42.5,
      "type": 0
    },
    {
      "id": 18059,
      "sensor_id": "16NFDT",
      "timestamp": 1525778576,
      "tempAir": 44.1,
      "humAir": 53,
      "type": 0
    },
    {
      "id": 18057,
      "sensor_id": "16NFDT",
      "timestamp": 1525778274,
      "tempAir": 46.2,
      "humAir": 46.5,
      "type": 0
    },
    {
      "id": 13946,
      "sensor_id": "16NFDT",
      "timestamp": 1525768930,
      "tempAir": 46.4,
      "humAir": 48,
      "type": 0
    },
    {
      "id": 14723,
      "sensor_id": "16NFDT",
      "timestamp": 1525770707,
      "tempAir": 50.9,
      "humAir": 42.5,
      "type": 0
    },
    {
      "id": 18755,
      "sensor_id": "16NFDT",
      "timestamp": 1525780362,
      "tempAir": 45,
      "humAir": 50,
      "type": 0
    },
    {
      "id": 18757,
      "sensor_id": "16NFDT",
      "timestamp": 1525780513,
      "tempAir": 42.3,
      "humAir": 58.5,
      "type": 0
    },
    {
      "id": 14722,
      "sensor_id": "16NFDT",
      "timestamp": 1525770556,
      "tempAir": 50.5,
      "humAir": 42.5,
      "type": 0
    },
    {
      "id": 14724,
      "sensor_id": "16NFDT",
      "timestamp": 1525770858,
      "tempAir": 51.1,
      "humAir": 42,
      "type": 0
    },
    {
      "id": 18756,
      "sensor_id": "16NFDT",
      "timestamp": 1525780211,
      "tempAir": 45.1,
      "humAir": 51,
      "type": 0
    },
    {
      "id": 18754,
      "sensor_id": "16NFDT",
      "timestamp": 1525780060,
      "tempAir": 45,
      "humAir": 50.5,
      "type": 0
    },
    {
      "id": 15518,
      "sensor_id": "16NFDT",
      "timestamp": 1525772635,
      "tempAir": 49.8,
      "humAir": 41.5,
      "type": 0
    },
    {
      "id": 15516,
      "sensor_id": "16NFDT",
      "timestamp": 1525772333,
      "tempAir": 50.9,
      "humAir": 42,
      "type": 0
    },
    {
      "id": 15517,
      "sensor_id": "16NFDT",
      "timestamp": 1525772484,
      "tempAir": 51.1,
      "humAir": 41,
      "type": 0
    },
    {
      "id": 15519,
      "sensor_id": "16NFDT",
      "timestamp": 1525772786,
      "tempAir": 50.4,
      "humAir": 41,
      "type": 0
    },
    {
      "id": 19663,
      "sensor_id": "16NFDT",
      "timestamp": 1525782453,
      "tempAir": 40.1,
      "humAir": 62.5,
      "type": 0
    },
    {
      "id": 20825,
      "sensor_id": "16NFDT",
      "timestamp": 1525785598,
      "tempAir": 58.1,
      "humAir": 36,
      "type": 14
    },
    {
      "id": 16600,
      "sensor_id": "16NFDT",
      "timestamp": 1525774712,
      "tempAir": 48,
      "humAir": 46.5,
      "type": 0
    },
    {
      "id": 17349,
      "sensor_id": "16NFDT",
      "timestamp": 1525776493,
      "tempAir": 45.7,
      "humAir": 50,
      "type": 0
    },
    {
      "id": 17350,
      "sensor_id": "16NFDT",
      "timestamp": 1525776644,
      "tempAir": 45.5,
      "humAir": 50,
      "type": 0
    },
    {
      "id": 23035,
      "sensor_id": "16NFDT",
      "timestamp": 1525791214,
      "tempAir": 70,
      "humAir": 28.5,
      "type": 0
    },
    {
      "id": 21561,
      "sensor_id": "16NFDT",
      "timestamp": 1525787369,
      "tempAir": 64.9,
      "humAir": 33,
      "type": 0
    },
    {
      "id": 23036,
      "sensor_id": "16NFDT",
      "timestamp": 1525791365,
      "tempAir": 71.2,
      "humAir": 28,
      "type": 0
    },
    {
      "id": 23033,
      "sensor_id": "16NFDT",
      "timestamp": 1525791063,
      "tempAir": 69.1,
      "humAir": 26,
      "type": 0
    },
    {
      "id": 23034,
      "sensor_id": "16NFDT",
      "timestamp": 1525790912,
      "tempAir": 70.7,
      "humAir": 27.5,
      "type": 0
    },
    {
      "id": 23987,
      "sensor_id": "16NFDT",
      "timestamp": 1525793284,
      "tempAir": 81,
      "humAir": 20.5,
      "type": 0
    },
    {
      "id": 22291,
      "sensor_id": "16NFDT",
      "timestamp": 1525789292,
      "tempAir": 69.8,
      "humAir": 26,
      "type": 0
    },
    {
      "id": 22290,
      "sensor_id": "16NFDT",
      "timestamp": 1525789141,
      "tempAir": 68.4,
      "humAir": 28.5,
      "type": 0
    },
    {
      "id": 22292,
      "sensor_id": "16NFDT",
      "timestamp": 1525789443,
      "tempAir": 68,
      "humAir": 28.5,
      "type": 0
    },
    {
      "id": 21562,
      "sensor_id": "16NFDT",
      "timestamp": 1525787520,
      "tempAir": 66,
      "humAir": 32,
      "type": 0
    },
    {
      "id": 24685,
      "sensor_id": "16NFDT",
      "timestamp": 1525795195,
      "tempAir": 83.5,
      "humAir": 18.5,
      "type": 0
    },
    {
      "id": 24686,
      "sensor_id": "16NFDT",
      "timestamp": 1525795044,
      "tempAir": 82.2,
      "humAir": 19.5,
      "type": 0
    },
    {
      "id": 25413,
      "sensor_id": "16NFDT",
      "timestamp": 1525796956,
      "tempAir": 83.3,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 25412,
      "sensor_id": "16NFDT",
      "timestamp": 1525797107,
      "tempAir": 81.5,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 26120,
      "sensor_id": "16NFDT",
      "timestamp": 1525798866,
      "tempAir": 82.9,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 26122,
      "sensor_id": "16NFDT",
      "timestamp": 1525799017,
      "tempAir": 81.9,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 26121,
      "sensor_id": "16NFDT",
      "timestamp": 1525798715,
      "tempAir": 82.6,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 27492,
      "sensor_id": "16NFDT",
      "timestamp": 1525802694,
      "tempAir": 86.7,
      "humAir": 11,
      "type": 0
    },
    {
      "id": 26805,
      "sensor_id": "16NFDT",
      "timestamp": 1525800779,
      "tempAir": 85.3,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 27491,
      "sensor_id": "16NFDT",
      "timestamp": 1525802543,
      "tempAir": 86.5,
      "humAir": 11,
      "type": 0
    },
    {
      "id": 27490,
      "sensor_id": "16NFDT",
      "timestamp": 1525802392,
      "tempAir": 85.1,
      "humAir": 13.5,
      "type": 0
    },
    {
      "id": 27717,
      "sensor_id": "16NFDT",
      "timestamp": 1525802845,
      "tempAir": 90.1,
      "humAir": 10,
      "type": 0
    },
    {
      "id": 26804,
      "sensor_id": "16NFDT",
      "timestamp": 1525800628,
      "tempAir": 84.7,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 26806,
      "sensor_id": "16NFDT",
      "timestamp": 1525800477,
      "tempAir": 84.4,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 26807,
      "sensor_id": "16NFDT",
      "timestamp": 1525800930,
      "tempAir": 84.4,
      "humAir": 13,
      "type": 0
    },
    {
      "id": 28402,
      "sensor_id": "16NFDT",
      "timestamp": 1525804758,
      "tempAir": 83.1,
      "humAir": 13,
      "type": 0
    },
    {
      "id": 29079,
      "sensor_id": "16NFDT",
      "timestamp": 1525806520,
      "tempAir": 88,
      "humAir": 9.5,
      "type": 0
    },
    {
      "id": 29080,
      "sensor_id": "16NFDT",
      "timestamp": 1525806671,
      "tempAir": 88.7,
      "humAir": 7,
      "type": 0
    },
    {
      "id": 30461,
      "sensor_id": "16NFDT",
      "timestamp": 1525810498,
      "tempAir": 89.4,
      "humAir": 6,
      "type": 0
    },
    {
      "id": 29746,
      "sensor_id": "16NFDT",
      "timestamp": 1525808586,
      "tempAir": 89.8,
      "humAir": 8.5,
      "type": 0
    },
    {
      "id": 29745,
      "sensor_id": "16NFDT",
      "timestamp": 1525808435,
      "tempAir": 88.5,
      "humAir": 7.5,
      "type": 0
    },
    {
      "id": 29744,
      "sensor_id": "16NFDT",
      "timestamp": 1525808284,
      "tempAir": 90.7,
      "humAir": 7,
      "type": 0
    },
    {
      "id": 31171,
      "sensor_id": "16NFDT",
      "timestamp": 1525812260,
      "tempAir": 90.7,
      "humAir": 7,
      "type": 0
    },
    {
      "id": 31170,
      "sensor_id": "16NFDT",
      "timestamp": 1525812109,
      "tempAir": 90.1,
      "humAir": 5.5,
      "type": 0
    },
    {
      "id": 31172,
      "sensor_id": "16NFDT",
      "timestamp": 1525812411,
      "tempAir": 89.6,
      "humAir": 8,
      "type": 0
    },
    {
      "id": 30460,
      "sensor_id": "16NFDT",
      "timestamp": 1525810347,
      "tempAir": 91,
      "humAir": 8,
      "type": 0
    },
    {
      "id": 31169,
      "sensor_id": "16NFDT",
      "timestamp": 1525811958,
      "tempAir": 90.7,
      "humAir": 6.5,
      "type": 0
    },
    {
      "id": 30458,
      "sensor_id": "16NFDT",
      "timestamp": 1525810045,
      "tempAir": 92.8,
      "humAir": 6.5,
      "type": 0
    },
    {
      "id": 30459,
      "sensor_id": "16NFDT",
      "timestamp": 1525810196,
      "tempAir": 91.6,
      "humAir": 7,
      "type": 0
    },
    {
      "id": 32074,
      "sensor_id": "16NFDT",
      "timestamp": 1525814322,
      "tempAir": 92.5,
      "humAir": 7,
      "type": 0
    },
    {
      "id": 32770,
      "sensor_id": "16NFDT",
      "timestamp": 1525816084,
      "tempAir": 91,
      "humAir": 7.5,
      "type": 0
    },
    {
      "id": 32771,
      "sensor_id": "16NFDT",
      "timestamp": 1525816235,
      "tempAir": 91.4,
      "humAir": 7,
      "type": 0
    },
    {
      "id": 33466,
      "sensor_id": "16NFDT",
      "timestamp": 1525817995,
      "tempAir": 93.2,
      "humAir": 8.5,
      "type": 0
    },
    {
      "id": 33467,
      "sensor_id": "16NFDT",
      "timestamp": 1525817844,
      "tempAir": 92.1,
      "humAir": 8.5,
      "type": 0
    },
    {
      "id": 33468,
      "sensor_id": "16NFDT",
      "timestamp": 1525818146,
      "tempAir": 93,
      "humAir": 8,
      "type": 0
    },
    {
      "id": 34172,
      "sensor_id": "16NFDT",
      "timestamp": 1525820058,
      "tempAir": 92.3,
      "humAir": 10,
      "type": 0
    },
    {
      "id": 34170,
      "sensor_id": "16NFDT",
      "timestamp": 1525819756,
      "tempAir": 91.2,
      "humAir": 8.5,
      "type": 0
    },
    {
      "id": 34171,
      "sensor_id": "16NFDT",
      "timestamp": 1525819907,
      "tempAir": 91.9,
      "humAir": 9.5,
      "type": 0
    },
    {
      "id": 34869,
      "sensor_id": "16NFDT",
      "timestamp": 1525821819,
      "tempAir": 89.6,
      "humAir": 9,
      "type": 0
    },
    {
      "id": 34867,
      "sensor_id": "16NFDT",
      "timestamp": 1525821517,
      "tempAir": 89.6,
      "humAir": 9.5,
      "type": 0
    },
    {
      "id": 34868,
      "sensor_id": "16NFDT",
      "timestamp": 1525821970,
      "tempAir": 90.5,
      "humAir": 11.5,
      "type": 0
    },
    {
      "id": 34870,
      "sensor_id": "16NFDT",
      "timestamp": 1525821668,
      "tempAir": 88.3,
      "humAir": 9.5,
      "type": 0
    },
    {
      "id": 40798,
      "sensor_id": "16NFDT",
      "timestamp": 1525837316,
      "tempAir": 63,
      "humAir": 26,
      "type": 0
    },
    {
      "id": 36461,
      "sensor_id": "16NFDT",
      "timestamp": 1525825645,
      "tempAir": 88.2,
      "humAir": 11,
      "type": 0
    },
    {
      "id": 36462,
      "sensor_id": "16NFDT",
      "timestamp": 1525825796,
      "tempAir": 88,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 37172,
      "sensor_id": "16NFDT",
      "timestamp": 1525827711,
      "tempAir": 85.5,
      "humAir": 12,
      "type": 0
    },
    {
      "id": 39463,
      "sensor_id": "16NFDT",
      "timestamp": 1525833470,
      "tempAir": 61.9,
      "humAir": 29,
      "type": 0
    },
    {
      "id": 37171,
      "sensor_id": "16NFDT",
      "timestamp": 1525827560,
      "tempAir": 86,
      "humAir": 12,
      "type": 0
    },
    {
      "id": 37869,
      "sensor_id": "16NFDT",
      "timestamp": 1525829325,
      "tempAir": 84.7,
      "humAir": 13,
      "type": 0
    },
    {
      "id": 37870,
      "sensor_id": "16NFDT",
      "timestamp": 1525829627,
      "tempAir": 85.1,
      "humAir": 14.5,
      "type": 0
    },
    {
      "id": 37868,
      "sensor_id": "16NFDT",
      "timestamp": 1525829476,
      "tempAir": 82.9,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 38576,
      "sensor_id": "16NFDT",
      "timestamp": 1525831545,
      "tempAir": 69.4,
      "humAir": 23,
      "type": 0
    },
    {
      "id": 38573,
      "sensor_id": "16NFDT",
      "timestamp": 1525831092,
      "tempAir": 84.7,
      "humAir": 14.5,
      "type": 0
    },
    {
      "id": 38575,
      "sensor_id": "16NFDT",
      "timestamp": 1525831394,
      "tempAir": 73.8,
      "humAir": 22,
      "type": 0
    },
    {
      "id": 38574,
      "sensor_id": "16NFDT",
      "timestamp": 1525831243,
      "tempAir": 81.3,
      "humAir": 17,
      "type": 0
    },
    {
      "id": 40136,
      "sensor_id": "16NFDT",
      "timestamp": 1525835392,
      "tempAir": 62.6,
      "humAir": 25.5,
      "type": 0
    },
    {
      "id": 40137,
      "sensor_id": "16NFDT",
      "timestamp": 1525835241,
      "tempAir": 62.6,
      "humAir": 25.5,
      "type": 0
    },
    {
      "id": 40797,
      "sensor_id": "16NFDT",
      "timestamp": 1525837165,
      "tempAir": 63.3,
      "humAir": 25,
      "type": 0
    },
    {
      "id": 35769,
      "sensor_id": "16NFDT",
      "timestamp": 1525823882,
      "tempAir": 90.3,
      "humAir": 10,
      "type": 0
    },
    {
      "id": 41485,
      "sensor_id": "16NFDT",
      "timestamp": 1525838936,
      "tempAir": 60.3,
      "humAir": 28,
      "type": 0
    },
    {
      "id": 41486,
      "sensor_id": "16NFDT",
      "timestamp": 1525839087,
      "tempAir": 61,
      "humAir": 28.5,
      "type": 0
    },
    {
      "id": 41487,
      "sensor_id": "16NFDT",
      "timestamp": 1525839238,
      "tempAir": 59,
      "humAir": 30,
      "type": 0
    },
    {
      "id": 42159,
      "sensor_id": "16NFDT",
      "timestamp": 1525840708,
      "tempAir": 57.9,
      "humAir": 31.5,
      "type": 0
    },
    {
      "id": 42160,
      "sensor_id": "16NFDT",
      "timestamp": 1525841010,
      "tempAir": 58.8,
      "humAir": 31.5,
      "type": 0
    },
    {
      "id": 42158,
      "sensor_id": "16NFDT",
      "timestamp": 1525840859,
      "tempAir": 57,
      "humAir": 32,
      "type": 0
    },
    {
      "id": 42161,
      "sensor_id": "16NFDT",
      "timestamp": 1525841161,
      "tempAir": 59,
      "humAir": 31,
      "type": 0
    },
    {
      "id": 43759,
      "sensor_id": "16NFDT",
      "timestamp": 1525844865,
      "tempAir": 53.1,
      "humAir": 39.5,
      "type": 0
    },
    {
      "id": 43760,
      "sensor_id": "16NFDT",
      "timestamp": 1525845016,
      "tempAir": 53.6,
      "humAir": 39,
      "type": 0
    },
    {
      "id": 43069,
      "sensor_id": "16NFDT",
      "timestamp": 1525843085,
      "tempAir": 56.1,
      "humAir": 35,
      "type": 0
    },
    {
      "id": 44451,
      "sensor_id": "16NFDT",
      "timestamp": 1525846646,
      "tempAir": 52.7,
      "humAir": 41,
      "type": 0
    },
    {
      "id": 44452,
      "sensor_id": "16NFDT",
      "timestamp": 1525846797,
      "tempAir": 50.7,
      "humAir": 45.5,
      "type": 0
    },
    {
      "id": 44453,
      "sensor_id": "16NFDT",
      "timestamp": 1525846948,
      "tempAir": 48.4,
      "humAir": 51,
      "type": 0
    },
    {
      "id": 45147,
      "sensor_id": "16NFDT",
      "timestamp": 1525848577,
      "tempAir": 51.8,
      "humAir": 42,
      "type": 0
    },
    {
      "id": 45149,
      "sensor_id": "16NFDT",
      "timestamp": 1525848879,
      "tempAir": 52.2,
      "humAir": 42,
      "type": 0
    },
    {
      "id": 45148,
      "sensor_id": "16NFDT",
      "timestamp": 1525848728,
      "tempAir": 51.8,
      "humAir": 42.5,
      "type": 0
    },
    {
      "id": 48823,
      "sensor_id": "16NFDT",
      "timestamp": 1525858533,
      "tempAir": 42.6,
      "humAir": 58.5,
      "type": 0
    },
    {
      "id": 48820,
      "sensor_id": "16NFDT",
      "timestamp": 1525858080,
      "tempAir": 48.4,
      "humAir": 48.5,
      "type": 0
    },
    {
      "id": 48822,
      "sensor_id": "16NFDT",
      "timestamp": 1525858382,
      "tempAir": 43.5,
      "humAir": 58.5,
      "type": 0
    },
    {
      "id": 50473,
      "sensor_id": "16NFDT",
      "timestamp": 1525862256,
      "tempAir": 42.4,
      "humAir": 57,
      "type": 0
    },
    {
      "id": 45837,
      "sensor_id": "16NFDT",
      "timestamp": 1525850509,
      "tempAir": 52.7,
      "humAir": 41,
      "type": 0
    },
    {
      "id": 45838,
      "sensor_id": "16NFDT",
      "timestamp": 1525850660,
      "tempAir": 52.3,
      "humAir": 41.5,
      "type": 0
    },
    {
      "id": 45840,
      "sensor_id": "16NFDT",
      "timestamp": 1525850358,
      "tempAir": 52.5,
      "humAir": 41.5,
      "type": 0
    },
    {
      "id": 45839,
      "sensor_id": "16NFDT",
      "timestamp": 1525850811,
      "tempAir": 50.9,
      "humAir": 46,
      "type": 0
    },
    {
      "id": 46768,
      "sensor_id": "16NFDT",
      "timestamp": 1525852739,
      "tempAir": 49.5,
      "humAir": 45,
      "type": 0
    },
    {
      "id": 50472,
      "sensor_id": "16NFDT",
      "timestamp": 1525862407,
      "tempAir": 42.8,
      "humAir": 56,
      "type": 0
    },
    {
      "id": 49747,
      "sensor_id": "16NFDT",
      "timestamp": 1525860469,
      "tempAir": 45,
      "humAir": 49.5,
      "type": 0
    },
    {
      "id": 47469,
      "sensor_id": "16NFDT",
      "timestamp": 1525854520,
      "tempAir": 51.6,
      "humAir": 41.5,
      "type": 0
    },
    {
      "id": 47470,
      "sensor_id": "16NFDT",
      "timestamp": 1525854671,
      "tempAir": 51.1,
      "humAir": 43,
      "type": 0
    },
    {
      "id": 48152,
      "sensor_id": "16NFDT",
      "timestamp": 1525856449,
      "tempAir": 50.7,
      "humAir": 42,
      "type": 0
    },
    {
      "id": 48151,
      "sensor_id": "16NFDT",
      "timestamp": 1525856298,
      "tempAir": 50,
      "humAir": 43.5,
      "type": 0
    },
    {
      "id": 48153,
      "sensor_id": "16NFDT",
      "timestamp": 1525856600,
      "tempAir": 50.7,
      "humAir": 39,
      "type": 0
    },
    {
      "id": 48821,
      "sensor_id": "16NFDT",
      "timestamp": 1525858231,
      "tempAir": 45.5,
      "humAir": 53,
      "type": 0
    },
    {
      "id": 51184,
      "sensor_id": "16NFDT",
      "timestamp": 1525864344,
      "tempAir": 43.3,
      "humAir": 57,
      "type": 0
    },
    {
      "id": 51182,
      "sensor_id": "16NFDT",
      "timestamp": 1525864042,
      "tempAir": 42.6,
      "humAir": 57.5,
      "type": 0
    },
    {
      "id": 51183,
      "sensor_id": "16NFDT",
      "timestamp": 1525864193,
      "tempAir": 43.2,
      "humAir": 57,
      "type": 0
    },
    {
      "id": 51868,
      "sensor_id": "16NFDT",
      "timestamp": 1525865980,
      "tempAir": 44.2,
      "humAir": 53.5,
      "type": 0
    },
    {
      "id": 51870,
      "sensor_id": "16NFDT",
      "timestamp": 1525866282,
      "tempAir": 45.3,
      "humAir": 51.5,
      "type": 0
    },
    {
      "id": 51869,
      "sensor_id": "16NFDT",
      "timestamp": 1525866131,
      "tempAir": 44.8,
      "humAir": 52.5,
      "type": 0
    },
    {
      "id": 56269,
      "sensor_id": "16NFDT",
      "timestamp": 1525876980,
      "tempAir": 70.5,
      "humAir": 25.5,
      "type": 0
    },
    {
      "id": 52596,
      "sensor_id": "16NFDT",
      "timestamp": 1525868065,
      "tempAir": 45.5,
      "humAir": 52.5,
      "type": 0
    },
    {
      "id": 54828,
      "sensor_id": "16NFDT",
      "timestamp": 1525873283,
      "tempAir": 59.2,
      "humAir": 37,
      "type": 0
    },
    {
      "id": 56270,
      "sensor_id": "16NFDT",
      "timestamp": 1525877131,
      "tempAir": 72.7,
      "humAir": 24,
      "type": 0
    },
    {
      "id": 54827,
      "sensor_id": "16NFDT",
      "timestamp": 1525873132,
      "tempAir": 55.9,
      "humAir": 43.5,
      "type": 0
    },
    {
      "id": 56268,
      "sensor_id": "16NFDT",
      "timestamp": 1525876678,
      "tempAir": 67.5,
      "humAir": 30.5,
      "type": 0
    },
    {
      "id": 52597,
      "sensor_id": "16NFDT",
      "timestamp": 1525868216,
      "tempAir": 45.5,
      "humAir": 51.5,
      "type": 0
    },
    {
      "id": 52595,
      "sensor_id": "16NFDT",
      "timestamp": 1525867914,
      "tempAir": 45.7,
      "humAir": 51.5,
      "type": 0
    },
    {
      "id": 52594,
      "sensor_id": "16NFDT",
      "timestamp": 1525867763,
      "tempAir": 45.7,
      "humAir": 51.5,
      "type": 0
    },
    {
      "id": 55570,
      "sensor_id": "16NFDT",
      "timestamp": 1525875208,
      "tempAir": 66.4,
      "humAir": 30,
      "type": 0
    },
    {
      "id": 55569,
      "sensor_id": "16NFDT",
      "timestamp": 1525875057,
      "tempAir": 67.8,
      "humAir": 29,
      "type": 0
    },
    {
      "id": 55568,
      "sensor_id": "16NFDT",
      "timestamp": 1525874906,
      "tempAir": 69.3,
      "humAir": 28.5,
      "type": 0
    },
    {
      "id": 53605,
      "sensor_id": "16NFDT",
      "timestamp": 1525870145,
      "tempAir": 55.8,
      "humAir": 39,
      "type": 0
    },
    {
      "id": 54096,
      "sensor_id": "16NFDT",
      "timestamp": 1525871358,
      "tempAir": 59,
      "humAir": 37,
      "type": 14
    },
    {
      "id": 56267,
      "sensor_id": "16NFDT",
      "timestamp": 1525876829,
      "tempAir": 67.8,
      "humAir": 28.5,
      "type": 0
    },
    {
      "id": 57145,
      "sensor_id": "16NFDT",
      "timestamp": 1525879051,
      "tempAir": 76.1,
      "humAir": 22,
      "type": 0
    },
    {
      "id": 57806,
      "sensor_id": "16NFDT",
      "timestamp": 1525880968,
      "tempAir": 81,
      "humAir": 18.5,
      "type": 0
    },
    {
      "id": 58489,
      "sensor_id": "16NFDT",
      "timestamp": 1525882877,
      "tempAir": 83.8,
      "humAir": 17,
      "type": 0
    },
    {
      "id": 59908,
      "sensor_id": "16NFDT",
      "timestamp": 1525886708,
      "tempAir": 97.5,
      "humAir": 13,
      "type": 0
    },
    {
      "id": 58488,
      "sensor_id": "16NFDT",
      "timestamp": 1525882726,
      "tempAir": 83.1,
      "humAir": 16,
      "type": 0
    },
    {
      "id": 59905,
      "sensor_id": "16NFDT",
      "timestamp": 1525886255,
      "tempAir": 89.8,
      "humAir": 14.5,
      "type": 0
    },
    {
      "id": 59906,
      "sensor_id": "16NFDT",
      "timestamp": 1525886406,
      "tempAir": 93.9,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 60631,
      "sensor_id": "16NFDT",
      "timestamp": 1525888320,
      "tempAir": 93.9,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 59189,
      "sensor_id": "16NFDT",
      "timestamp": 1525884491,
      "tempAir": 87.1,
      "humAir": 16,
      "type": 0
    },
    {
      "id": 59191,
      "sensor_id": "16NFDT",
      "timestamp": 1525884793,
      "tempAir": 91,
      "humAir": 14.5,
      "type": 0
    },
    {
      "id": 59190,
      "sensor_id": "16NFDT",
      "timestamp": 1525884642,
      "tempAir": 88.9,
      "humAir": 15,
      "type": 0
    },
    {
      "id": 60632,
      "sensor_id": "16NFDT",
      "timestamp": 1525888622,
      "tempAir": 95.2,
      "humAir": 9.5,
      "type": 0
    },
    {
      "id": 60630,
      "sensor_id": "16NFDT",
      "timestamp": 1525888169,
      "tempAir": 92.1,
      "humAir": 14,
      "type": 0
    },
    {
      "id": 59907,
      "sensor_id": "16NFDT",
      "timestamp": 1525886557,
      "tempAir": 94.5,
      "humAir": 14.5,
      "type": 0
    },
    {
      "id": 60633,
      "sensor_id": "16NFDT",
      "timestamp": 1525888471,
      "tempAir": 97.9,
      "humAir": 11.5,
      "type": 0
    },
    {
      "id": 61588,
      "sensor_id": "16NFDT",
      "timestamp": 1525890536,
      "tempAir": 95.2,
      "humAir": 9,
      "type": 0
    },
    {
      "id": 63086,
      "sensor_id": "16NFDT",
      "timestamp": 1525894365,
      "tempAir": 92.8,
      "humAir": 11.5,
      "type": 0
    },
    {
      "id": 63085,
      "sensor_id": "16NFDT",
      "timestamp": 1525894063,
      "tempAir": 90,
      "humAir": 13,
      "type": 0
    },
    {
      "id": 63850,
      "sensor_id": "16NFDT",
      "timestamp": 1525896281,
      "tempAir": 94.6,
      "humAir": 9,
      "type": 0
    },
    {
      "id": 63084,
      "sensor_id": "16NFDT",
      "timestamp": 1525894214,
      "tempAir": 91.2,
      "humAir": 11.5,
      "type": 0
    },
    {
      "id": 62324,
      "sensor_id": "16NFDT",
      "timestamp": 1525892450,
      "tempAir": 93.4,
      "humAir": 13,
      "type": 0
    },
    {
      "id": 62323,
      "sensor_id": "16NFDT",
      "timestamp": 1525892299,
      "tempAir": 92.3,
      "humAir": 11,
      "type": 0
    },
    {
      "id": 63848,
      "sensor_id": "16NFDT",
      "timestamp": 1525896130,
      "tempAir": 90.7,
      "humAir": 10.5,
      "type": 0
    },
    {
      "id": 63849,
      "sensor_id": "16NFDT",
      "timestamp": 1525895979,
      "tempAir": 92.7,
      "humAir": 10.5,
      "type": 0
    },
    {
      "id": 64595,
      "sensor_id": "16NFDT",
      "timestamp": 1525898196,
      "tempAir": 95.2,
      "humAir": 11,
      "type": 0
    },
    {
      "id": 64593,
      "sensor_id": "16NFDT",
      "timestamp": 1525897894,
      "tempAir": 91,
      "humAir": 9.5,
      "type": 0
    },
    {
      "id": 64594,
      "sensor_id": "16NFDT",
      "timestamp": 1525898045,
      "tempAir": 94.1,
      "humAir": 9.5,
      "type": 0
    },
    {
      "id": 64592,
      "sensor_id": "16NFDT",
      "timestamp": 1525897743,
      "tempAir": 95,
      "humAir": 8.5,
      "type": 0
    },
    {
      "id": 65496,
      "sensor_id": "16NFDT",
      "timestamp": 1525899959,
      "tempAir": 89.8,
      "humAir": 9.5,
      "type": 0
    },
    {
      "id": 65494,
      "sensor_id": "16NFDT",
      "timestamp": 1525899657,
      "tempAir": 91,
      "humAir": 11.5,
      "type": 0
    },
    {
      "id": 65495,
      "sensor_id": "16NFDT",
      "timestamp": 1525899808,
      "tempAir": 92.1,
      "humAir": 11,
      "type": 0
    }
  ];