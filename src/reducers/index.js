import { combineReducers } from 'redux';
import sensorsReducer from './sensorsReducer';
import markerReducer from './markerReducer';
import loginReducer from './loginReducer';
import userReducer from './userReducer';

export default combineReducers({
  sensors: sensorsReducer,
  markers: markerReducer,
  login: loginReducer,
  appUser: userReducer
});