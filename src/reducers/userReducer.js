import {
  SET_LOGIN_STATUS,
  SET_USERNAME,
  SET_USERID,
  SET_CURRENT_VIEW,
  SET_EMAIL,
  SET_PHONE,
  SET_ADDRESS,
  SET_FARM_NAME,
  SET_COORD,
  SET_DEVICES,
  SET_MISSING_DEVICES
} from '../actions/userActions';

export default (state = {
  isLoggedIn: false,
  userName: null,
  userID: null,
  phone: null,
  address: null,
  email: null,
  farmName: null,
  coord: null,
  devices: null,
  missingDevices: null,
  currentView: null
}, action) => {
  switch (action.type) {
    case SET_LOGIN_STATUS:
      return Object.assign({}, state, {
        isLoggedIn: action.isLoggedIn
      });

    case SET_USERNAME:
      return Object.assign({}, state, {
        userName: action.userName
      });

    case SET_USERID:
      return Object.assign({}, state, {
        userID: action.userID
      });
    case SET_PHONE:
      return Object.assign({}, state, {
        phone: action.phone
      });
    case SET_EMAIL:
      return Object.assign({}, state, {
        email: action.email
      });
    case SET_ADDRESS:
      return Object.assign({}, state, {
        address: action.address
      });
    case SET_FARM_NAME:
      return Object.assign({}, state, {
        farmName: action.farmName
      });
    case SET_COORD:
      return Object.assign({}, state, {
        coord: action.coord
      });
    case SET_DEVICES:
      return Object.assign({}, state, {
        devices: action.devices
      });
    case SET_MISSING_DEVICES:
      return Object.assign({}, state, {
        missingdDevices: action.missingDevices
      });
    case SET_CURRENT_VIEW:
      return Object.assign({}, state, {
        currentView: action.currentView
      });

    default:
      return state;
  }
}