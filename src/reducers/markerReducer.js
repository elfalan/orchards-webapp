import sensors from '../assets/sensors';
import _ from 'lodash';
import { SET_ACTIVE_MARKER, SET_LATEST_DATA } from '../actions/markerActions';

const getIntialMarkerState = () => {
    let markers = [];
    _.each(sensors, sensor => {
        markers.push({id: sensor.id, lat: sensor.lat, lng: sensor.lng, isOpen: false});
    });    
    return markers;
}

const initialState = {
    activeMarker: getIntialMarkerState(),
    latestData: []
}

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_ACTIVE_MARKER:
            return {...state, activeMarker: action.markers };
        //let id = action.id;
        //   return Object.assign({}, state, {
        //        [id]: Object.assign({}, state[id], {isOpen: true})
        // });
        case SET_LATEST_DATA:
        return {...state, latestData: state.latestData.concat(action.latestData) };
        default:
            return state;
    }
}