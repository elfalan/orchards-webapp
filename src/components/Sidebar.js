import React from "react";
import PropTypes from "prop-types";
//import { NavLink } from "react-router-dom";
//import cx from "classnames";
import Paper from 'material-ui/Paper';
import {List, ListItem} from 'material-ui/List';
import Menu  from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Assessment from 'material-ui/svg-icons/action/assessment';
import Place from 'material-ui/svg-icons/maps/place';
import AccountCircle from 'material-ui/svg-icons/action/account-circle';
import Settings from 'material-ui/svg-icons/action/settings';
import Timeline from 'material-ui/svg-icons/action/timeline';
import Divider from 'material-ui/Divider';

//import { HeaderLinks } from "../../components";
const style = {
  paper: {
    display: 'inline-block',
    float: 'left',
    margin: '16px 32px 16px 0',
  },
};

export default class Sidebar extends React.Component{

render(){
    return(
        <div>
      <Paper style={style.paper}>
      <Menu>
       <MenuItem primaryText="Profile" onItemClick={this.props.handleItemClick("Profile")} leftIcon={<AccountCircle />} />
        <MenuItem primaryText="Dashboard" onItemClick={this.props.handleItemClick("Dashboard")} leftIcon={<Assessment />} />
        <MenuItem primaryText="Sensor Map" onItemClick={this.props.handleItemClick("Map")} leftIcon={<Place />} />
        <MenuItem primaryText="Sensor Data" onItemClick={this.props.handleItemClick("Data")} leftIcon={<Timeline />} />
        <MenuItem primaryText="Settings" onItemClick={this.props.handleItemClick("Settings")} leftIcon={<Settings />} />
      </Menu>
      </Paper>
      </div>
    
  );
}  
}
