import React from 'react';
import { compose, withProps } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps"

const MyMapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAuao3jKDSSPepEWWhQk2C7SoAWW8cOHdY&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={12}
    // defaultCenter={{ lat: 38.823, lng: -107.825 }}
    defaultCenter={{ lat: props.coord.lat, lng: props.coord.lng }}
    defaultMapTypeId={`satellite`}
    onClick={props.handleMapClick}
  >
    {props.children}
  </GoogleMap>
);

export default MyMapComponent;