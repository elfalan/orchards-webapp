import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
//import { login } from '../../reducers/loginReducer';
import { setLoginPending, setLoginError, setLoginSuccess } from '../../actions/loginActions';
import {
    setCurrentView,
    setLoginStatus,
   // setUserID,
    setUsername,
    setEmail,
    setPhone,
    setAddress,
    setCoord,
    setFarmName,
    setDevices
} from '../../actions/userActions';

import { Redirect } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton/RaisedButton';
//import syLogo from '../../assets/img/sy-orch-logo.png';
//import syLogo from '../../assets/img/sy_logo_sm.png';
import TextField from 'material-ui/TextField';
import CircularProgress from 'material-ui/CircularProgress';
import Center from 'react-center';
import './LoginForm.css';

const style = {
    margin: 35,
};

class LoginForm extends Component {

    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
    }

    render() {
        //let { email, password } = this.state;
        const { isLoginPending, isLoginSuccess, loginError } = this.props;
        const { from } = this.props.location.state || { from: { pathname: "/map" } }
        console.log("from: " + JSON.stringify(from));
        console.log("loginsuccess: " + isLoginSuccess);
        if (isLoginSuccess) {
            //this.props.setUsername(this.state.email); 
            this.props.setCurrentView(from.pathname.replace('/', ''));
            this.props.setLoginStatus(true);
            // return (<Redirect to="/map" />);
            return (<Redirect to={from} />);
        }
        return (
            <Center>
                <div className="Container" >
                    <Center>
                        {/* <img name="logo" src={require('../../assets/img/sy-orch-logo.png')} /> */}
                        <img name="logo" src={require('../../assets/img/sy_logo_sm.png')} />
                    </Center>
                    <Center>
                        <form name="loginForm" onSubmit={this.onSubmit}>
                            <TextField
                                hintText="Enter Your Email"
                                floatingLabelText="Email"
                                type="email"
                                name="email"
                                onChange={e => this.setState({ email: e.target.value })}
                            />
                            <TextField
                                hintText="Enter Your Password"
                                floatingLabelText="Password"
                                type="password"
                                name="password"
                                onChange={e => this.setState({ password: e.target.value })}
                            />

                            {/* <div className="inputButton">
                <input type="submit" value="Login" />
                </div> */}
                            <Center>
                                <RaisedButton type="submit" label="Login" primary={true} style={style} />
                            </Center>
                        </form>
                    </Center>
                    <Center>
                        <div className="message">
                            {isLoginPending && <CircularProgress size={40} thickness={7} />}
                            {isLoginSuccess && <div>Success.</div>}
                            {loginError && <div>{loginError.message}</div>}
                        </div>
                    </Center>
                </div>
            </Center>

        )
    }

    onSubmit(e) {
        e.preventDefault();
        let { email, password } = this.state;
        this.props.login(email, password);
        this.setState({
            email: '',
            password: '',
        });
    }

}


const login = (email, password) => {
    return dispatch => {
        dispatch(setLoginPending(true));
        dispatch(setLoginSuccess(false));
        dispatch(setLoginError(null));

        callLoginApi(email, password, payload => {
            dispatch(setLoginPending(false));
            console.log("payload: " + JSON.stringify(payload));
            if (!(payload.type instanceof Error)) {
                console.log("payload: " + JSON.stringify(payload));
                dispatch(setLoginSuccess(true));
                //populate user stores
                dispatch(setUsername(payload.userName));
                dispatch(setEmail(payload.email));
                dispatch(setPhone(payload.phone));
                dispatch(setAddress(payload.address));
                dispatch(setFarmName(payload.farmName));
                dispatch(setCoord(payload.coord));
                dispatch(setDevices(payload.devices));

            } else {
                dispatch(setLoginError(payload.type));
            }
        });
    }
}

const backendLogin = async (email, password) => {
    try {
        const results = await axios.post(`https://staging.map-backend.smartyields.com/login`, {
            'email': email,
            'password': password
        });
        return results.data;
    } catch (e) {
        console.log(e.message);
    }
}

const getUserInfo = async (token) => {
    console.log("before try");
    try {
        console.log(JSON.stringify(token));
        const results = await axios.post(`https://staging.map-backend.smartyields.com/me`, {
            'headers': {
                'Authorization': `Bearer ${token}`
            }
        }, {});
        console.log("user data: " + JSON.stringify(results.data));
        return results.data;
    } catch (e) {
        console.log(e.message);
    }
    console.log("after try");
}

const adminGetRequest = () => {
          const requestStr = "https://staging.map-backend.smartyields.com/users?token=JR8d7sDf79RbBgmussPSnv5EQSyK8nrs"
          axios.get(requestStr)
                .then(response => {
                    return response.data;
                });
}

const callLoginApi = (email, password, callback) => {
  setTimeout(() => {
    axios.post('https://staging.map-backend.smartyields.com/login', {
        email: email,
        password: password
        //email: 'brandon.fowler@smartyields.com',
        //password: 'smart808'
    }).
        then(response => {
            console.log("status: " + response.status);
            console.log(response);
            if (response.status === 200) {

                const token = response.data;
                // const AuthStr = 'Bearer ' + token;

                // axios.post(`https://staging.map-backend.smartyields.com/me`, {
                //     'headers': {
                //         'Authorization': `Bearer ${token}`
                //     }
                // }, {})
                
                const requestStr = "https://staging.map-backend.smartyields.com/users?token=JR8d7sDf79RbBgmussPSnv5EQSyK8nrs"
                axios.get(requestStr)
                .then(results => {
                    console.log("results: " + JSON.stringify(results));
                    const userData = results.data.filter(item => {
                                         console.log("email: " + JSON.stringify(item.email));
                                         return item.email === email;
                                     });
                    const data = userData[0];
                    return callback({
                        type: true,
                        userName: data.info.name,
                        email: data.email,
                        phone: data.info.cellphone,
                        address: data.info.address,
                        farmName: data.info.farm,
                        coord: data.info.location,
                        devices: data.alert.activeDevices
                    });
                })
                    .catch(error => {
                        console.error(error);
                        return callback({ type: new Error('Server or Network Error') });
                    })
                }
            })
                    .catch(error => {
                        console.error(error);
                        return callback({ type: new Error('Invalid Email or Password') });
                    });

        //Admin get
        //   const requestStr = "https://staging.map-backend.smartyields.com/users?token=JR8d7sDf79RbBgmussPSnv5EQSyK8nrs"
        //   axios.get(requestStr)
        //         .then(response => {
        //             //console.log("user data: " + JSON.stringify(response));
        //             const data = response.data;
        //             const userData = data.filter(item => {
        //                 console.log("email: " + JSON.stringify(item.email));
        //                 return item.email === email;
        //             });
        //             console.log("userdata: " + JSON.stringify(userData));
        //             return callback({type: true,
        //                              userName: userData[0].info.name,
        //                              email: userData[0].email,
        //                              phone: userData[0].info.cellphone,
        //                              address: userData[0].info.address,
        //                              farmName: userData[0].info.farm,
        //                              coord: userData[0].info.location,
        //                              devices: userData[0].alert.activeDevices
        //                              });
        //         })
        //         .catch(error => {
        //             console.error(error);
        //             return callback({type: new Error('Server or Network Error')});
        //         }); 

        //     }
        // })
       

       }, 1000);
}


const mapStateToProps = (state) => {
    return {
        isLoginPending: state.login.isLoginPending,
        isLoginSuccess: state.login.isLoginSuccess,
        loginError: state.login.loginError
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: (email, password) => dispatch(login(email, password)),
        setCurrentView: (path) => dispatch(setCurrentView(path)),
        setLoginStatus: (status) => dispatch(setLoginStatus(status))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);