import React from 'react';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import Sidebar from '../components/Sidebar';
import IconButton from 'material-ui/IconButton';
import RaisedButton from 'material-ui/RaisedButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

import Paper from 'material-ui/Paper';
import {List, ListItem} from 'material-ui/List';
import Menu  from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Assessment from 'material-ui/svg-icons/action/assessment';
import Place from 'material-ui/svg-icons/maps/place';
import AccountCircle from 'material-ui/svg-icons/action/account-circle';
import Settings from 'material-ui/svg-icons/action/settings';
import Timeline from 'material-ui/svg-icons/action/timeline';
import Divider from 'material-ui/Divider';



const style = {
  paper: {
    display: 'inline-block',
    float: 'left',
    margin: '16px 32px 16px 0',
  },
};

class AppBarMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
  }

  handleDrawerToggle = () => this.setState({ open: !this.state.open });

  handleClose = () => this.setState({ open: false });

  render() {
    return (
      <div>
        <AppBar
          title="Sensor Map"
          onLeftIconButtonClick={this.handleDrawerToggle}
        />
        <Drawer
          docked={false}
          width={200}
          open={this.state.open}
          onRequestChange={(open) => this.setState({ open })}
        >
          <div>
            <Paper style={style.paper}>
              <Menu>
                <MenuItem primaryText="Profile" leftIcon={<AccountCircle />} />
                <MenuItem primaryText="Dashboard"  leftIcon={<Assessment />} />
                <MenuItem primaryText="Sensor Map"  leftIcon={<Place />} />
                <MenuItem primaryText="Sensor Data"  leftIcon={<Timeline />} />
                <MenuItem primaryText="Settings"  leftIcon={<Settings />} />
              </Menu>
            </Paper>
          </div>
          {/* <Sidebar handleItemClick={this.handleItemClick} /> */}
        </Drawer>
      </div>
    );
  }


}

export default AppBarMenu;

