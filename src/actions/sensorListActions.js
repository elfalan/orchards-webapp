export const SHOW_SENSOR_DATA = 'SHOW_SENSOR_DATA';
export const ADD_SENSOR_FIELDS = `ADD_SENSOR_FIELDS`;


export const showSensorData = (id) => {
    console.log("dispatch : selected id: " + id);
return {
    type: SHOW_SENSOR_DATA,
    id
}
}

export const addSensorFields = (_id, sensorType, lat, lng, farmName) => {

return {
  type: ADD_SENSOR_FIELDS,
  sensorListItem: {_id, sensorType, lat, lng, farmName }
}
}

