export const SET_ACTIVE_MARKER = `SET_ACTIVE_MARKER`;
export const SET_LATEST_DATA = `SET_LATEST_DATA`;

export const setLatestData = (id, temp, hum) => {
    return {
        type: SET_LATEST_DATA,
        latestData: {id, temp, hum }
      }
}
// export const setLatestData = (id, temp, hum) => {
//     return (dispatch, getState) => {
//         const latestData = getState().markers.latestData.filter(item => {
//             return item.id != id;
//         });
//         latestData.push({id, temp, hum });
//         dispatch({
//             type: SET_LATEST_DATA,
//             markers: latestData
//         });
//     }
// }

export const setActiveMarker = (id, lat, lng, isOpen) => {
    return (dispatch, getState) => {
        const activeMarker = getState().markers.activeMarker.filter(marker => {
            return marker.id !== id;
        });

        activeMarker.push({ id, lat, lng, isOpen });
        dispatch({
            type: SET_ACTIVE_MARKER,
            markers: activeMarker
        });
    }
}
