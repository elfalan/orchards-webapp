export const SET_LOGIN_STATUS = `SET_LOGIN_STATUS`;
export const SET_USERNAME = `SET_USERNAME`;
export const SET_EMAIL = `SET_EMAIL`;
export const SET_PHONE = `SET_PHONE`;
export const SET_ADDRESS = `SET_ADDRESS`;
export const SET_FARM_NAME = `SET_FARM_NAME`;
export const SET_COORD = `SET_COORD`;
export const SET_DEVICES = `SET_DEVICES`;
export const SET_MISSING_DEVICES = `SET_MISSING_DEVICES`;
export const SET_USERID = `SET_USERID`;
export const SET_CURRENT_VIEW = `SET_CURRENT_VIEW`;
//export const SET_AUTH_TOKEN = `SET_AUTH_TOKEN`;

export const setLoginStatus = (isLoggedIn) => {
    return{
        type: SET_LOGIN_STATUS,
        isLoggedIn
    };
}

export const setUsername = (userName) => {
    return{
        type: SET_USERNAME,
        userName
    }
}
export const setUserID = (userID) => {
    return{
        type: SET_USERID,
        userID
    }
}

export const setEmail = (email) => {
    return{
        type: SET_EMAIL,
        email
    }
}

export const setPhone = (phone) => {
    return{
        type: SET_PHONE,
        phone
    }
}

export const setAddress = (address) => {
    return{
        type: SET_ADDRESS,
        address
    }
}

export const setFarmName = (farmName) => {
    return{
        type: SET_FARM_NAME,
        farmName
    }
}
export const setCoord = (coord) => {
    return{
        type: SET_COORD,
        coord
    }
}
export const setDevices = (devices) => {
    return{
        type: SET_DEVICES,
        devices
    }
}
export const setMissingDevices = (missingDevices) => {
    return{
        type: SET_MISSING_DEVICES,
        missingDevices
    }
}

export const setCurrentView = (currentView) => {
    return{
        type: SET_CURRENT_VIEW,
        currentView
    }
}

// export const setAuthToken = (token) => {
//     return{
//         type: SET_AUTH_TOKEN,
//         token
//     }
// }

