export const SET_LOGIN_PENDING = `SET_LOGIN_PENDING`;
export const SET_LOGIN_SUCCESS = `SET_LOGIN_SUCCESS`;
export const SET_LOGIN_ERROR = `SET_LOGIN_ERROR`;

export const setLoginPending = (isLoginPending) => {
  return {
    type: SET_LOGIN_PENDING,
    isLoginPending
  };
}

export const setLoginSuccess = (isLoginSuccess) => {
  return {
    type: SET_LOGIN_SUCCESS,
    isLoginSuccess
  };
}

export const setLoginError = (loginError) => {
  return {
    type: SET_LOGIN_ERROR,
    loginError
  }
}


// export const login = (email, password) => {
//   return dispatch => {
//     dispatch(setLoginPending(true));
//     dispatch(setLoginSuccess(false));
//     dispatch(setLoginError(null));

//     callLoginApi(email, password, error => {
//       dispatch(setLoginPending(false));
//       if (!error) {
//         dispatch(setLoginSuccess(true));
//       } else {
//         dispatch(setLoginError(error));
//       }
//     });
//   }
// }

// export const callLoginApi = (email, password, callback) => {
//   setTimeout(() => {
//     if (email === 'admin@example.com' && password === 'admin') {
//       return callback(null);
//     } else {
//       return callback(new Error('Invalid email and password'));
//     }
//   }, 1000);
// }
