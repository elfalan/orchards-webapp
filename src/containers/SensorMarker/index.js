import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Marker, InfoWindow } from 'react-google-maps';

import { setActiveSensor, removeActiveSensor } from '../../actions/mapActions';
import { setActiveMarker } from '../../actions/markerActions';

const style = {
  tag: {
    fontSize: '15px',
    fontWeight: '500'
  }
}

class SensorMarker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.id,
      isOpen: false
    }

    this.handleMarkerClick = this.handleMarkerClick.bind(this);
    this.handleMarkerClose = this.handleMarkerClose.bind(this);
  }



  handleMarkerClick(id, lat, lng) {
    this.props.setActiveMarker(id, lat, lng, true);
  }

  handleMarkerClose(id, lat, lng) {
   this.props.setActiveMarker(id, lat, lng, false);
  }

  render() {
    const {
      id,
      lat,
      lng,
      isOpen,
      data
    } = this.props;

    const latestData = data.filter(item => {
      return item.id === id;
    });
    console.warn(JSON.stringify(latestData));

    return (
      <Marker position={{ lat, lng }} onClick={() => this.handleMarkerClick(id, lat, lng, isOpen)}>
        {isOpen && 
        <InfoWindow onCloseClick={() => this.handleMarkerClose(id, lat, lng, isOpen)}>
          <div>
          <div><text style={style.tag}>{id}</text></div>
          <div>{latestData[0].temp} °F</div>
          <div>{latestData[0].hum} %</div>
          </div>
        </InfoWindow>}
      </Marker>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.markers.latestData
    // id: state.markers.id,
    // lat: state.makers.lat,
    // lng: state.markers.lng,
    // isActive: state.markers.isActive
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setActiveSensor: (id) => {
      dispatch(setActiveSensor(id));
    },
    removeActiveSensor: () => {
      dispatch(removeActiveSensor());
    },
    setActiveMarker: (id, lat, lng, isOpen) => {
      dispatch(setActiveMarker(id, lat, lng, isOpen));
    }
  }
}

const ConnectedSensorMarker = connect(
  mapStateToProps,
  mapDispatchToProps
)(SensorMarker);

export default ConnectedSensorMarker;