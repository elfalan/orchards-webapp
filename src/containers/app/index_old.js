import React, { Component } from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBarHeader from '../../components/AppBarHeader';
import Map from '../Map';
//import SensorList from '../SensorList';
import ConnectedSensorList from '../SensorListContainer';
import SensorDataPanel from '../SensorDataPanelContainer';
import '../SensorList/styles.css';

class App extends Component {
  render() {
    return (
      <div className="App">
     
      <MuiThemeProvider>
        <AppBarHeader />
        router
        <Map />
        <ConnectedSensorList />
        {/* <SensorDataPanel /> */}
        {/* <SensorList /> */}
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
