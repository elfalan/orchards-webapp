import React, { Component } from 'react';
//Redux
import { connect } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
//Appheader
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Place from 'material-ui/svg-icons/maps/place';
import AccountCircle from 'material-ui/svg-icons/action/account-circle';
import Logout from 'material-ui/svg-icons/navigation/arrow-back';
import Divider from 'material-ui/Divider';
import Center from 'react-center';

//Container Views and Components
import LoginForm from '../../components/LoginForm/LoginForm';
import ConnectedSensorList from '../ConnectedSensorList';
import Map from '../Map';
import UserProfile from '../Profile';

//Store actions
import { setCurrentView, setLoginStatus, setUserID, setUsername } from '../../actions/userActions';
import { setLoginSuccess } from '../../actions/loginActions';

import { BrowserRouter as Router, Route, Link, Redirect } from 'react-router-dom';

const style = {
  paper: {
    display: 'inline-block',
    float: 'left',
    margin: '16px 32px 16px 0',
  },
  link: {
    textDecoration: 'none',
    color: 'black',
    fontSize: '20px',
    fontWeight: '400'

  },
  menuItem: {
    paddingBottom:'10px',
    paddingTop: '10px'
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    paddingBottom: '15px',
    paddingTop: '10px'
  },
  logo: {
    width: '70%'
  }
};

//const initialPage = "Map";

class App extends Component {

  // componentDidMount() {
  //   this.setState({title: this.props.location.state});
  // }


  constructor(props) {
    super(props);
    this.state = {
      open: false,
      // title: initialPage,
    };
  }

  handleDrawerToggle = () => this.setState({ open: !this.state.open });

  handleClose = () => this.setState({ open: false });

  isAppBarVisible = (validLogin) => {
    if (validLogin) {
      return true;
    }
    return false;
  }

  handleLinkClick = (text) => {
    this.changeTitle(text);
    this.handleDrawerToggle();    // this.setState({title: text});
  }

  changeTitle = (text) => {
    this.props.setCurrentView(text);
  }

  logout = () => {
    this.handleDrawerToggle();
    this.props.setLoginSuccess(false);
    return (
      <Redirect
        to={{
          pathname: "/",
          // state: { from: this.props.location }
        }}
      />
    );
  }

  render() {
    const { validLogin, loginError, currentView } = this.props;
    console.log("valid login: " + JSON.stringify(validLogin));

    return (
      <div className="App" >
        <MuiThemeProvider>
          <AppBar
            title={currentView}
            onLeftIconButtonClick={this.handleDrawerToggle}
            style={{ visibility: this.isAppBarVisible(validLogin) ? "visible" : "hidden" }}

          />
          <Router>
            <div>
              <Drawer
                docked={false}
                width={200}
                open={this.state.open}
                onRequestChange={(open) => this.setState({ open })}
              >
                <div>
                  {/* <Paper style={style.paper}> */}
                  
                    <div style={style.wrapper}>
                    <Center>
                      <img style={style.logo} src={require('../../assets/img/sy_logo_sm.png')} />
                      </Center>
                    </div>
                  
                  <Divider />
                  <Menu>
                    {/* <MenuItem primaryText={<Link onClick={() => this.handleLinkClick("Login")} style={style.link} to="/">Login</Link>} leftIcon={<Assessment />} /> */}

                    <MenuItem style={style.menuItem} primaryText={<Link onClick={() => this.handleLinkClick("map")} style={style.link} to="/map">map</Link>} leftIcon={<Place />} />

                    <MenuItem style={style.menuItem} primaryText={<Link onClick={() => this.handleLinkClick("profile")} style={style.link} to="/profile">profile</Link>} leftIcon={<AccountCircle />} />

                    <MenuItem style={style.menuItem} primaryText={<Link onClick={() => this.logout()} style={style.link} to="/">logout</Link>} leftIcon={<Logout />} />
                    
                    {/* <MenuItem primaryText={<Link onClick={() => this.handleLinkClick("Data Trends")} style={style.link} to="/data">Data</Link>} leftIcon={<Timeline />} />

                      <MenuItem primaryText={<Link onClick={() => this.handleLinkClick("Settings")} style={style.link} to="/settings">Settings</Link>} leftIcon={<Settings />} /> */}
                  </Menu>

                  {/* </Paper> */}
                </div>
              </Drawer>

              <Route exact path="/" component={LoginForm} />
              {/* <Route exact path="/map" component={map} /> */}

              <PrivateRoute path="/map" component={map} loginStatus={validLogin} />
              <PrivateRoute exact path="/profile" component={profile} loginStatus={validLogin} />
              {/* <Route exact path="/logout" component={logout} render={(this.props) => <logout {...this.props} />}/> */}
            </div>
          </Router>
        </MuiThemeProvider>

      </div>
    );
  }
}


const PrivateRoute = ({ component: Component, loginStatus, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      // fakeAuth.isAuthenticated ? (
      loginStatus ? (
        <Component {...props} />
      ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location }
            }}
          />
        )
    }
  />
);

const map = () => {
  return (
    <div>
      <Map />
      <ConnectedSensorList />
    </div>
  );
};



const profile = () => {

  return (
    <div>
      <UserProfile />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    loginStatus: state.appUser.isLoggedIn,
    validLogin: state.login.isLoginSuccess,
    loginError: state.login.loginError,
    currentView: state.appUser.currentView
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentView: (path) => dispatch(setCurrentView(path)),
    setLoginSuccess: (val) => dispatch(setLoginSuccess(val))
    //setLoginStatus: (val) => dispatch(setLoginStatus(val))

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
