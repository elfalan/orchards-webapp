import React, { Component } from 'react';
import { connect } from 'react-redux';
//import { showSensorData } from '../../actions/sensorListActions';
import Dialog from 'material-ui/Dialog';
import DatePicker from 'material-ui/DatePicker';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import axios from 'axios';
import Center from 'react-center';
import _ from 'lodash';
import { CSVLink } from 'react-csv';
import { setLatestData } from '../../actions/markerActions';

//import sampleData from '../../assets/16NFDT_sample';
//import allSensorData from '../../assets/sensorData';
import ChartistGraph from 'react-chartist';
//import 'chartist/dist/scss/chartist.scss';
import '../../assets/css/chartist.css';


const style = {
  dialog: {
    width: '80%',
    height: '100%',
    maxWidth: 'none',
  },
  chart: {
    height: '400px',
    
  },
  block: {
    display: 'inline-block',
    padding: '15px',
  },
  dataStatus: {
    font: '15px'
  },
  sensorTitleStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    display:'flex',
    color: '#00B8D4',
    fontWeight: '600',
    fontSize: '25px'
  },
 
}

let hours = null;

class SensorView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      startTime: 0,
      endTime: (new Date()).getHours(),
      startDate: new Date(),
      endDate: new Date(),
      sensorData: [], 
    };

    hours = this.createHours();
  }
  
  //initialize component with data from previous day
  componentWillMount = async () => {
     let start = this.state.startDate;
     //start.setDate(start.getDate()-1); //subtract 1 day
     start.setHours(start.getHours()-this.state.endTime); //from beginning of today

     start = Math.floor(start.getTime()/1000);
     let end = Math.floor(this.state.endDate.getTime()/1000);
     console.log(JSON.stringify(end));
     await this.getSensorData(start,end);
  }

  createHours = () => {
    let arr = [];
    for(let i = 0; i < 24; i ++){
      let obj = {};
      if(i < 12){
        if(i === 0){
          obj.hour = 12 + " am";
        }else{
          obj.hour = i + " am";
        }
      }else{
        if(i === 12){
          obj.hour = i + " pm";
        }else{
          obj.hour = (i-12) + " pm";
        }
      }
      obj.key = i;
      arr.push(obj);
    }
    return arr;
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleStartTimeChange = (event, index, value) => {
    this.setState({ startTime: value });
  };

  handleEndTimeChange = (event, index, value) => {
    this.setState({ endTime: value });
  };

  handleStartDateChange = (event, date) => {
    this.setState({startDate: date});
  };

  handleEndDateChange = (event, date) => {
    this.setState({endDate: date});
  };

  refreshSensorData = () => {
    let startEvent = this.state.startDate;
    //let sensorID = this.props.sensorID;
    startEvent.setHours(this.state.startTime,0,0);

    let endEvent = this.state.endDate;
    endEvent.setHours(this.state.endTime,0,0);
    
    let unixStart = Math.floor(startEvent.getTime() / 1000);
    let unixEnd = Math.floor(endEvent.getTime() / 1000);
    
    console.log("start: " + startEvent + " \nend: "  + endEvent);
    console.log("unix start: " + unixStart);
    console.log("unix end: " + unixEnd);
   
    this.getSensorData(unixStart, unixEnd);
 
  }

  getSensorData = async (unixStart, unixEnd) => {
    let sensorID = this.props.sensorID;
    
    let requestStr = "http://staging.alert-backend.smartyields.com/api/devices/"+sensorID 
    +"/data?token=1LT7PzT@ZOm22T546h25qzu@7%3X6wVY&from=" + unixStart + "&to=" + unixEnd;                 
    console.log(requestStr);
    await axios.get(requestStr).then(responseData => {
    this.setState({sensorData: responseData.data });

        //DEV
      //  console.log("data: " + responseData.data);
        let latestTemp = _.last(responseData.data).tempAir;
        let latestHum = _.last(responseData.data).humAir;
        console.log("latest: " + sensorID + " , " + latestTemp + " , " + latestHum);
        this.props.setLatestData(sensorID, latestTemp, latestHum);
    });
  }

  convertTimestamp = (timestamp) => {
    let a = new Date(timestamp * 1000);
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    //let year = a.getFullYear();
    let month = months[a.getMonth()];
    let date = a.getDate();
    let hour = a.getHours();
    return month + date + " hr " + hour;
  }

  isStatusVisible = () => {
    if(this.state.sensorData.length === 0){return true;}
    return false;
  }

  render() {
    const sensorID = this.props.sensorID;
    const sensorTitle = sensorID;
    const hourMenuItems = [];
    

    //Add Menu Items
    for (let i = 0; i < 24; i++) {
      let hourStr = hours[i].hour;
      hourMenuItems.push(<MenuItem value={i} key={i} primaryText={hourStr} />);
    }

    let tempData = [], humData = [], timestamps = [];
    //add data to arrays
     _.each(this.state.sensorData, item => {
      let dateString = this.convertTimestamp(item.timestamp);
      timestamps.push(dateString);
      tempData.push(item.tempAir);
      humData.push(item.humAir);
    });

    let scaleHigh = Math.max(tempData) + 10;
    let scaleLow = 0;

    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        onClick={this.handleClose}
      />,
    ];

    const biPolarChartData = {
      labels: timestamps,
      series: [tempData, humData]
    };

    const iFactor = 30;
    const type = 'Line';

    const options = {
      low: scaleLow,
      high: scaleHigh,
      showArea: true,
      showPoint: true,
      axisX: {
        labelInterpolationFnc: function (value, index) {
          return index % iFactor === 0 ? value : null;
        }
      }
    };

    return (
      <div>
        <RaisedButton label="See More" secondary={true} onClick={this.handleOpen} />
        <Dialog
          title={sensorTitle}
          titleStyle={style.sensorTitleStyle}
          actions={actions}
          modal={false}
          open={this.state.open}
          contentStyle={style.dialog}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <div>
            <div>
            <ChartistGraph data={biPolarChartData} options={options} type={type} style={style.chart} />
            <Center><div style={style.dataStatus}><text style={{visibility: this.isStatusVisible() ? "visible" : "hidden"}}>No Data Available</text></div></Center>
            </div>
            <Center>
            <div style={style.block}>
              <div style={style.block}>
                <DatePicker hintText="Select Start Date" container="inline" mode="landscape" value={this.state.startDate} onChange={this.handleStartDateChange}/>
                <SelectField
                  value={this.state.startTime}
                  onChange={this.handleStartTimeChange}
                  maxHeight={200}
                >
                  {hourMenuItems}
                </SelectField>
              </div>
              <div style={style.block}>
                <DatePicker hintText="Select End Date" container="inline" mode="landscape"  value={this.state.endDate} onChange={this.handleEndDateChange}/>
                <SelectField
                  value={this.state.endTime}
                  onChange={this.handleEndTimeChange}
                  maxHeight={200}
                >
                  {hourMenuItems}
                </SelectField>
              </div>
             
            </div>
            <Center>
            <RaisedButton label="Refresh" secondary={true} onClick={() => this.refreshSensorData(this.props.sensorID)} />
            <CSVLink data={this.state.sensorData} ><RaisedButton label="Download" primary={true} /></CSVLink>
            </Center>
            </Center>
          </div>
          <div> 
          {/* <RaisedButton label="Download" primary={true}></RaisedButton>  */}
           
          </div>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {

  }
}

const mapDispatchToProps = dispatch => {
  return {
    setLatestData: (id, temp, hum) => dispatch(setLatestData(id, temp, hum))
  }
}

const SensorDataPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(SensorView);

export default SensorDataPanel;
