import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

class SensorList extends Component {
  
  state = {
    fixedHeader: true,
    fixedFooter: true,
    stripedRows: false,
    showRowHover: true,
    selectable: true,
    multiSelectable: true,
    enableSelectAll: true,
    deselectOnClickaway: true,
    showCheckboxes: true,
    height: '100%',
  };

  isSelected = (index) => {
    return this.state.selected.indexOf(index) !== -1;
  };

  handleRowSelection = (selectedRows) => {
    this.setState({
      selected: selectedRows,
    });
  };

  handleChange = (event) => {
    this.setState({height: event.target.value});
  };

  
  render() {
    const {
      sensors
    } = this.props;



    return (
      <Table
      height={this.state.height}
          fixedHeader={this.state.fixedHeader}
          fixedFooter={this.state.fixedFooter}
          selectable={this.state.selectable}
          multiSelectable={this.state.multiSelectable}
          >
      <TableHeader
      displaySelectAll={this.state.showCheckboxes}
      adjustForCheckbox={this.state.showCheckboxes}
      enableSelectAll={this.state.enableSelectAll}
      >
      <TableRow>
            <TableHeaderColumn>ID</TableHeaderColumn>
            <TableHeaderColumn>Type</TableHeaderColumn>
            <TableHeaderColumn>Latitude</TableHeaderColumn>
            <TableHeaderColumn>Longitude</TableHeaderColumn>
            <TableHeaderColumn>Farm</TableHeaderColumn>
            <TableHeaderColumn>Label</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody
        displayRowCheckbox={this.state.showCheckboxes}
        deselectOnClickaway={this.state.deselectOnClickaway}
        showRowHover={this.state.showRowHover}
        stripedRows={this.state.stripedRows}
        >         
           {sensors.map(sensor => { 
              return (
           <TableRow key={sensor.id}>  

                  <TableRowColumn>{sensor.id}</TableRowColumn>
                  <TableRowColumn>{sensor.type}</TableRowColumn>
                  <TableRowColumn>{sensor.lat}</TableRowColumn>
                  <TableRowColumn>{sensor.lng}</TableRowColumn>
                  <TableRowColumn>{sensor.farm}</TableRowColumn>
                  <TableRowColumn>{sensor.label}</TableRowColumn>
          </TableRow>
           );
          })}
          </TableBody>
      </Table>

      // <div id='sensors_table_container'>
      //   <table id='sensors_table'>
      //     <thead>
      //       <tr>
      //         <th>ID</th>
      //         <th>Type</th>
      //         <th>Latitude</th>
      //         <th>Longitude</th>
      //         <th>Farm</th>
      //         <th>Label</th>
      //       </tr>
      //     </thead>
      //     <tbody>
      //       {sensors.map(sensor => {
      //         return (
      //           <tr key={sensor.id}>
      //             <td>{sensor.id}</td>
      //             <td>{sensor.type}</td>
      //             <td>{sensor.lat}</td>
      //             <td>{sensor.lng}</td>
      //             <td>{sensor.farm}</td>
      //             <td>{sensor.label}</td>
      //           </tr>
      //         );
      //       })}
      //     </tbody>
      //   </table>
      // </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    sensors: state.sensors.sensors
  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

const ConnectedSensorList = connect(
  mapStateToProps,
  mapDispatchToProps
)(SensorList);

export default ConnectedSensorList;