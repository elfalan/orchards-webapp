import React, { Component } from 'react';
import { connect } from 'react-redux';

import MapComponent from '../../components/Map';
import SensorMarker from '../SensorMarker';
//import NewSensorMarker from '../NewSensorMarker';
import _ from 'lodash';

import { addSensor } from '../../actions/mapActions';


class Map extends Component {
  constructor(props) {
    super(props);

    this.handleMapClick = this.handleMapClick.bind(this);
  }

  handleMapClick(event) {
    // this.props.addSensor(event.latLng.lat(), event.latLng.lng());
  }

  render() {
    const {
      //sensors,
   //   newSensors,
      activeSensor,
      activeMarker,
   //   coord,
   //   userDevices,
      userMarkers
    } = this.props;

    //Test Map Coord
   

    console.log("USER MARKERS: " + JSON.stringify(userMarkers));

    const target = activeMarker[0];
    const targetCoord = { lat: target.lat, lng: target.lng };

    return (
      <MapComponent handleMapClick={this.handleMapClick} activeSensor={activeSensor} coord={targetCoord}>
        {userMarkers.map(marker =>
          <SensorMarker key={marker.id} {...marker} />
        )}
        {/* {activeMarker.map(marker => 
          <SensorMarker key={marker.id} {...marker} />
        )} */}
        {/* {sensors.map(sensor => 
          <SensorMarker key={sensor.id} {...sensor} />
        )} */}
        {/* {newSensors.map((sensor, index) =>
          <NewSensorMarker key={sensor.id} {...sensor} />
        )} */}
      </MapComponent>
    )
  }
}

const filterUserMarkers = (activeMarker, userDevices) => {
  let markerids = [];
  let userMarkers = [];
  _.each(activeMarker, marker => { markerids.push(marker.id) });
  //console.log("marker ids: " + JSON.stringify(markerids));
  //console.log("user Devices: " + JSON.stringify(userDevices));
  const intersection = _.intersection(markerids, userDevices);
  const difference = _.difference(userDevices, intersection);
  //console.log("intersection: " + JSON.stringify(intersection));
  console.warn("missing User devices: " + JSON.stringify(difference));

  _.each(activeMarker, marker => {
    if (intersection.includes(marker.id)) {
      userMarkers.push(marker);
    }
  });

  return userMarkers;
}

const mapStateToProps = state => {
  const userMarkers = filterUserMarkers(state.markers.activeMarker,state.appUser.devices);

  return {
    sensors: state.sensors.sensors,
    newSensors: state.sensors.newSensors,
    activeSensor: state.sensors.activeSensor,
    activeMarker: state.markers.activeMarker,
    coord: state.appUser.coord,
    userDevices: state.appUser.devices,
    userMarkers: userMarkers
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addSensor: (lat, lng) => {
      dispatch(addSensor(lat, lng));
    }
  }
}

const ConnectedMap = connect(
  mapStateToProps,
  mapDispatchToProps
)(Map);

export default ConnectedMap;