import React, { Component } from 'react';
import { connect } from 'react-redux';

import TextField from 'material-ui/TextField';
import Chip from 'material-ui/Chip';
import _ from 'lodash';

const style = {
    field: {
        display: 'block',
        padding: '12px',
    },
    label: {
        fontSize: '18px'
    },
    wrapper: {
        Display: 'flex',
        flexWrap: 'wrap',
    },
   
    chip: {
        margin: 4,
        Display: 'inline-block'
    }
}

class ProfileView extends Component {

    render() {
        const { appUser } = this.props;
        const userName = (appUser.userName !== undefined) ? appUser.userName : "undefined";
        const phone = (appUser.phone !== undefined) ? appUser.phone : "undefined";
        const email = (appUser.email !== undefined) ? appUser.email : "undefined";
        const farmName = (appUser.farmName !== undefined) ? appUser.farmName : "undefined";
        const address = (appUser.address !== undefined) ? appUser.address : "undefined";
        const devices = (appUser.devices !== undefined) ? appUser.devices : "undefined";
        const coord = (appUser.coord != undefined) ? appUser.coord.lat + ", " + appUser.coord.lng : "undefined"; //lat lng
        
        let deviceArr = (_.replace(devices, new RegExp("]","g"),"")).trim('"').split(',');
        // console.log("devices: " + JSON.stringify(devices));
        // _.each(deviceArr, device => {
        //     console.log(JSON.stringify(device));
        // });

        return (
            <div>
                <div>
                    <TextField
                        disabled={false}
                        value={userName}
                        floatingLabelText="Name"
                        floatingLabelStyle={style.label}
                        style={style.field}
                    />
                </div>
                <div>
                    <TextField
                        disabled={false}
                        value={phone}
                        floatingLabelText="Phone Number"
                        floatingLabelStyle={style.label}
                        style={style.field}
                    />
                </div>
                <div>
                    <TextField
                        disabled={false}
                        value={email}
                        floatingLabelText="Email"
                        floatingLabelStyle={style.label}
                        style={style.field}
                    />
                </div>
                <div>
                    <TextField
                        disabled={false}
                        value={farmName}
                        floatingLabelText="Farm Name"
                        floatingLabelStyle={style.label}
                        style={style.field}
                    />
                </div>
                <div>
                    <TextField
                        disabled={false}
                        value={address}
                        floatingLabelText="Address"
                        floatingLabelStyle={style.label}
                        style={style.field}
                    />
                </div>
                <div>
                    <TextField
                        disabled={false}
                        value={coord}
                        floatingLabelText="Location Coordinates"
                        floatingLabelStyle={style.label}
                        style={style.field}
                    />
                </div>
                <div style={style.wrapper}>
                
                     <TextField
                        disabled={true}
                      //  value={devices}
                        floatingLabelText="Devices"
                        floatingLabelStyle={style.label}
                        style={style.field}
                        />
                    {
                    deviceArr.map((device, index) => {
                        return(
                        <Chip
                       key={index}
                    //    onRequestDelete={() => this.handleRequestDelete(data.key)}
                       style={style.chip}
                     >
                       {device}
                     </Chip>
                        );
                    })}
                    
                </div>
            </div>

        );
    }

}

const mapStateToProps = state => {
    return {
        appUser: state.appUser
    }
}

const mapDispatchToProps = dispatch => {
    return {
    }
}

const userProfile = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileView);

export default userProfile;