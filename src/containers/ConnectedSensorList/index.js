import React, { Component } from 'react';
import { connect } from 'react-redux'
import { showSensorData } from '../../actions/sensorListActions';
import { setActiveSensor } from '../../actions/mapActions';
import { setActiveMarker } from '../../actions/markerActions';
import { setMissingDevices } from '../../actions/userActions';
import SensorDataPanel from '../SensorDataPanel';
import Center from 'react-center';


import _ from 'lodash';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

const style = {
  header: {
    font: '30px',
    fontFamily: 'Arial',
    color: '#FFFFFF',
  },
  banner: {
    justifyContent: 'center',
    alignItems: 'center',
    Display: 'flex',
    backgroundColor: '#464646',
    width: '100%',
    overflow: 'hidden',
    padding: '5px',
    flex: '1'
  }
}

class SensorTable extends Component {

  state = {
    selected: [null],
    fixedHeader: true,
    fixedFooter: true,
    stripedRows: false,
    showRowHover: true,
    selectable: true,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: false,
    showCheckboxes: true,
    height: '100%',
  };

  componentWillMount = () => {
    console.warn("list mounted");
  }

  isSelected = (index, id) => {
    let rowState = this.state.selected.indexOf(index);
    //let marker = userMarkers[index];
    if (rowState !== -1) {
      return true;
    }
    //  this.props.setActiveMarker(marker.id, marker.lat, marker.lng, false);
  };

  isVisible = (index) => {
    if (this.state.selected === index) {
      return true;
    }
    return false;
  }

  handleRowSelection = (selectedRowIndex) => {
    this.setState({
      selected: selectedRowIndex,
    });
    if (this.props.sensors[selectedRowIndex] !== undefined) {
      let sensor = this.props.userSensors[selectedRowIndex];
      console.log(sensor.id);
      this.props.setActiveMarker(sensor.id, sensor.lat, sensor.lng, true);
    }
  };

  handleChange = (event) => {
    this.setState({ height: event.target.value });
  };

  isTableVisible = () => {
    if(this.props.missingDevices.length > 0){
      return true;
    }
    return false;
  }

  render() {
    const {
      //  sensors,
      //  activeMarker,
      //  userDevices,
      userSensors,
      missingDevices
      } = this.props;

    return (
      <div>
        <div>
          <Table
            onRowSelection={this.handleRowSelection}
            height={this.state.height}
            fixedHeader={this.state.fixedHeader}
            fixedFooter={this.state.fixedFooter}
            selectable={this.state.selectable}
            multiSelectable={this.state.multiSelectable}
          >
            <TableHeader
              displaySelectAll={this.state.showCheckboxes}
              adjustForCheckbox={this.state.showCheckboxes}
              enableSelectAll={this.state.enableSelectAll}
            >
              <TableRow>
                <TableHeaderColumn>ID</TableHeaderColumn>
                <TableHeaderColumn>Type</TableHeaderColumn>
                <TableHeaderColumn>Latitude</TableHeaderColumn>
                <TableHeaderColumn>Longitude</TableHeaderColumn>
                <TableHeaderColumn>Farm</TableHeaderColumn>
                <TableHeaderColumn>Label</TableHeaderColumn>
                <TableHeaderColumn>Actions</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody
              displayRowCheckbox={this.state.showCheckboxes}
              deselectOnClickaway={this.state.deselectOnClickaway}
              showRowHover={this.state.showRowHover}
              stripedRows={this.state.stripedRows}
            >
              {
                userSensors.map((sensor, index) => {
                  return (
                    <TableRow key={sensor.id} selected={this.isSelected(index, sensor.id)}>

                      <TableRowColumn>{sensor.id}</TableRowColumn>
                      <TableRowColumn>{sensor.type}</TableRowColumn>
                      <TableRowColumn>{sensor.lat}</TableRowColumn>
                      <TableRowColumn>{sensor.lng}</TableRowColumn>
                      <TableRowColumn>{sensor.farm}</TableRowColumn>
                      <TableRowColumn>{sensor.label}</TableRowColumn>
                      <TableRowColumn>
                        {/* <div style={{visibility: this.isVisible(index) ? "visible" : "hidden"}}> */}
                        <SensorDataPanel sensorID={sensor.id} />
                        {/* </div> */}
                      </TableRowColumn>

                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </div>

        <div style={{visibility: this.isTableVisible() ? 'visible':'hidden'}}>
          <Center>
            <div style={style.banner}><Center><text style={style.header}>Devices Not Found</text></Center></div>
          </Center>
          <Table
            name="notFound"
            // onRowSelection={this.handleRowSelection}
            height={this.state.height}
            fixedHeader={true}
            fixedFooter={true}
          // selectable={this.state.selectable}
          // multiSelectable={this.state.multiSelectable}
          >
            <TableHeader
              displaySelectAll={false}
              adjustForCheckbox={false}
              enableSelectAll={false}
            >
              <TableRow>
                <TableHeaderColumn>ID</TableHeaderColumn>
                <TableHeaderColumn>Type</TableHeaderColumn>
                <TableHeaderColumn>Latitude</TableHeaderColumn>
                <TableHeaderColumn>Longitude</TableHeaderColumn>
                <TableHeaderColumn>Farm</TableHeaderColumn>
                <TableHeaderColumn>Label</TableHeaderColumn>
                <TableHeaderColumn>Actions</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody
              displayRowCheckbox={false}
              deselectOnClickaway={false}
              showRowHover={false}
              stripedRows={false}
            >
              {
                missingDevices.map((sensor, index) => {
                  return (
                    <TableRow key={sensor} selected={null}>

                      <TableRowColumn>{sensor}</TableRowColumn>
                      <TableRowColumn>{null}</TableRowColumn>
                      <TableRowColumn>{null}</TableRowColumn>
                      <TableRowColumn>{null}</TableRowColumn>
                      <TableRowColumn>{null}</TableRowColumn>
                      <TableRowColumn>{null}</TableRowColumn>
                      <TableRowColumn>{null}</TableRowColumn>

                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>

        </div>
      </div>
    );
  }
}

const filterUserSensors = (sensors, userDevices) => {
  //Filter User Markers
  let sensorids = [];
  let userSensors = [];

  _.each(sensors, sensor => { sensorids.push(sensor.id) });
  const intersection = _.intersection(sensorids, userDevices);
  const difference = _.difference(userDevices, intersection);

  _.each(sensors, sensor => {
    if (intersection.includes(sensor.id)) {
      userSensors.push(sensor);
    }
  });

  return { user: userSensors, missing: difference };
}


const mapStateToProps = state => {
  const filterObj = filterUserSensors(state.sensors.sensors, state.appUser.devices);
  const userSensors = filterObj.user;
  const missingDevices = filterObj.missing;

  return {
    sensors: state.sensors.sensors,
    activeSensor: state.sensors.activeSensor,
    activeMarker: state.markers.activeMarker,
    userDevices: state.appUser.devices,
    userSensors: userSensors,
    missingDevices: missingDevices
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    showSensorData: (id) => {
      dispatch(showSensorData(id))
    },
    setActiveSensor: (id) => {
      dispatch(setActiveSensor(id))
    },
    setActiveMarker: (id, lat, lng, isOpen) => {
      dispatch(setActiveMarker(id, lat, lng, isOpen))
    }
  }
}

const ConnectedSensorList = connect(
  mapStateToProps,
  mapDispatchToProps
)(SensorTable)

export default ConnectedSensorList;
